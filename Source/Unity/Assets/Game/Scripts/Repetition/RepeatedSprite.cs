﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepeatedSprite.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Repetition
{
    using Assets.Game.Scripts.Rendering.Behaviours;

    using UnityEngine;

    public class RepeatedSprite : MonoBehaviour
    {
        #region Fields

        public SpriteRenderer Current;

        private SpriteRenderer next;

        private SpriteRenderer previous;

        #endregion

        #region Methods

        private void CreateNext()
        {
            var nextSpriteObject = (GameObject)Instantiate(this.Current.gameObject);
            nextSpriteObject.transform.localPosition =
                new Vector3(
                    this.Current.transform.localPosition.x
                    + this.Current.sprite.bounds.size.x * this.Current.transform.localScale.x,
                    this.Current.transform.localPosition.y,
                    this.Current.transform.localPosition.z);
            nextSpriteObject.transform.parent = this.transform;
            nextSpriteObject.name = "Next";
            this.next = nextSpriteObject.GetComponent<SpriteRenderer>();

            // Hook up event handlers.
            var visibilityChangedEventBehaviour = this.next.GetComponent<VisibilityChangedEventBehaviour>();
            visibilityChangedEventBehaviour.BecameVisible += this.OnNextBecameVisible;
        }

        private void OnNextBecameVisible(GameObject obj)
        {
            var visibilityChangedEventBehaviour = this.next.GetComponent<VisibilityChangedEventBehaviour>();
            visibilityChangedEventBehaviour.BecameVisible -= this.OnNextBecameVisible;

            // Destroy previous.
            Destroy(this.previous.gameObject);

            // Update pointers.
            this.previous = this.Current;
            this.previous.gameObject.name = "Previous";

            this.Current = this.next;
            this.Current.gameObject.name = "Current";

            // Create new next.
            this.CreateNext();
        }

        private void Start()
        {
            // Create previous and next. 
            var previousSpriteObject = (GameObject)Instantiate(this.Current.gameObject);
            previousSpriteObject.transform.localPosition =
                new Vector3(
                    this.Current.transform.localPosition.x - this.Current.sprite.bounds.size.x,
                    this.Current.transform.localPosition.y,
                    this.Current.transform.localPosition.z);
            previousSpriteObject.transform.parent = this.transform;
            previousSpriteObject.name = "Previous";
            this.previous = previousSpriteObject.GetComponent<SpriteRenderer>();

            this.CreateNext();
        }

        #endregion
    }
}