﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntityDataBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Data
{
    using Slash.ECS;
    using Slash.ECS.Blueprints;
    using Slash.Unity.Common.ECS;

    using UnityEngine;

    public abstract class EntityDataBehaviour : MonoBehaviour
    {
        #region Fields

        private GameBehaviour gameBehaviour;

        #endregion

        #region Public Properties

        public int EntityId { get; private set; }

        #endregion

        #region Methods

        protected abstract Blueprint GetBlueprint();

        private void Awake()
        {
            this.gameBehaviour = FindObjectOfType<GameBehaviour>();

            this.gameBehaviour.GameChanged += this.OnGameChanged;
        }

        private void OnGameChanged(Game newGame, Game oldGame)
        {
            if (newGame != null)
            {
                var blueprint = this.GetBlueprint();
                this.EntityId = this.gameBehaviour.Game.EntityManager.CreateEntity(blueprint);

                // Register entity object.
                EntityGameObjectMap.Instance[this.EntityId] = this.gameObject;
            }
        }

        #endregion
    }
}