﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerEntityBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Data
{
    using Slash.ECS.Blueprints;

    public class PlayerEntityBehaviour : EntityDataBehaviour
    {
        #region Fields

        public int PlayerIndex;

        public int PlayerPawnIndex;

        #endregion

        #region Methods

        protected override Blueprint GetBlueprint()
        {
            var blueprint = new Blueprint();

            //blueprint.ComponentTypes.Add(typeof(PlayerOwnerComponent));
            //blueprint.ComponentTypes.Add(typeof(PlayerPawnComponent));
            //blueprint.ComponentTypes.Add(typeof(InventoryComponent));
            //blueprint.ComponentTypes.Add(typeof(PawnTargetZoneRelationComponent));

            //blueprint.AttributeTable.Add(PlayerOwnerComponent.AttributePlayerOwner, this.PlayerIndex);
            //blueprint.AttributeTable.Add(PlayerPawnComponent.AttributePawnIndex, this.PlayerPawnIndex);

            return blueprint;
        }

        #endregion
    }
}