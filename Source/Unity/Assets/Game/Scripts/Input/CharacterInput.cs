﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterInput.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Input
{
#if !UNITY_WEBPLAYER
    using XInputDotNetPure;
#endif

    using Slash.Unity.Common.ECS;

    using UnityEngine;

    using Event = Slash.ECS.Events.GameEvent;

    public class CharacterInput : GameEventBehaviour
    {
        #region Fields

        public int InputIndex;

        #endregion

        #region Public Properties

        public bool DashPressed
        {
            get
            {
#if !UNITY_WEBPLAYER
                //ButtonState buttonState = this.GamePadState.Buttons.B;
                //if (buttonState == ButtonState.Pressed)
                //{
                //    return true;
                //}
#endif
                var input = "Dash";
                if (Input.GetButtonDown(input))
                {
                    return true;
                }

                return false;
            }
        }

        public bool JumpPressed
        {
            get
            {
#if !UNITY_WEBPLAYER
                //ButtonState buttonState = this.GamePadState.Buttons.A;
                //if (buttonState == ButtonState.Pressed)
                //{
                //    return true;
                //}
#endif
                var input = "Jump";
                if (Input.GetButtonDown(input))
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Properties

#if !UNITY_WEBPLAYER
        private GamePadState GamePadState
        {
            get
            {
                return GamePad.GetState((PlayerIndex)this.InputIndex);
            }
        }
#endif

        #endregion
    }
}