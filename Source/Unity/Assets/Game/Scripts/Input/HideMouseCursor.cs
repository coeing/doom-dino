﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HideMouseCursor.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Input
{
    using UnityEngine;

    public class HideMouseCursor : MonoBehaviour
    {
        #region Methods

        private void OnDisable()
        {
            Screen.showCursor = true;
        }

        private void OnEnable()
        {
            Screen.showCursor = false;
        }

        #endregion
    }
}