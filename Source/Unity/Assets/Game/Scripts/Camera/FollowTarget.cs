﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FollowTarget.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Assets.Game.Scripts.Camera
{
    using UnityEngine;

    public class FollowTarget : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Offset to target position.
        /// </summary>
        public Vector3 Offset;

        /// <summary>
        ///   Target to follow.
        /// </summary>
        public Transform Target;

        #endregion

        #region Methods

        [ContextMenu("Update position")]
        private void LateUpdate()
        {
            if (this.Target != null)
            {
                this.transform.position = this.Target.position + this.Offset;
            }
        }

        #endregion
    }
}