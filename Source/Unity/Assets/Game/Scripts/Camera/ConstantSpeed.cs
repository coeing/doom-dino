﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstantSpeed.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Assets.Game.Scripts.Camera
{
    using UnityEngine;

    public class ConstantSpeed : MonoBehaviour
    {
        #region Fields

        public Vector2 Speed;

        public Transform Target;

        #endregion

        #region Methods

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            Vector2 positionDelta = this.Speed * Time.deltaTime;
            this.Target.position = this.Target.position + new Vector3(positionDelta.x, positionDelta.y, 0);
        }

        #endregion
    }
}