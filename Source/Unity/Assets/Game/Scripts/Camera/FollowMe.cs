﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FollowMe.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Assets.Game.Scripts.Camera
{
    using UnityEngine;

    public class FollowMe : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Target to follow.
        /// </summary>
        public Transform Camera;

        public bool IgnoreX;

        public bool IgnoreY;

        public bool IgnoreZ;

        /// <summary>
        ///   Offset to target position.
        /// </summary>
        public Vector3 Offset;

        #endregion

        #region Methods

        [ContextMenu("Update position")]
        private void LateUpdate()
        {
            if (this.Camera != null)
            {
                Vector3 position = this.transform.position + this.Offset;
                Vector3 newPosition = new Vector3(
                    this.IgnoreX ? this.Camera.position.x : position.x,
                    this.IgnoreY ? this.Camera.position.y : position.y,
                    this.IgnoreZ ? this.Camera.position.z : position.z);
                this.Camera.position = newPosition;
            }
        }

        private void Start()
        {
            this.Camera = UnityEngine.Camera.main.transform;
        }

        #endregion
    }
}