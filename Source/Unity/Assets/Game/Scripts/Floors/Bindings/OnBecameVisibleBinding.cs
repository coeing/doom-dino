﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OnBecameVisibleBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Bindings
{
    public class OnBecameVisibleBinding : NguiCommandBinding
    {
        #region Methods

        private void OnBecameVisible()
        {
            if (this._command == null)
            {
                return;
            }

            this._command.DynamicInvoke();
        }

        #endregion
    }
}