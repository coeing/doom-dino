﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColliderHeightBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Bindings
{
    using UnityEngine;

    public class ColliderHeightBinding : NguiNumericBinding
    {
        #region Fields

        private BoxCollider2D boxCollider2D;

        #endregion

        #region Public Methods and Operators

        public override void Awake()
        {
            base.Awake();

            this.boxCollider2D = this.GetComponent<BoxCollider2D>();
        }

        #endregion

        #region Methods

        protected override void ApplyNewValue(double val)
        {
            this.boxCollider2D.center = new Vector2(0, (float)val);
        }

        #endregion
    }
}