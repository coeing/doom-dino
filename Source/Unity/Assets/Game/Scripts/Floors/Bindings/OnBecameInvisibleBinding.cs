﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OnBecameInvisibleBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Bindings
{
    public class OnBecameInvisibleBinding : NguiCommandBinding
    {
        #region Methods

        private void OnBecameInvisible()
        {
            if (this._command == null)
            {
                return;
            }

            this._command.DynamicInvoke();
        }

        #endregion
    }
}