﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitySpriteBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Bindings
{
    using UnityEngine;

    public class UnitySpriteBinding : NguiTextBinding
    {
        #region Fields

        private SpriteRenderer spriteRenderer;

        private BoxCollider2D boxCollider2D;

        #endregion

        #region Public Methods and Operators

        public override void Awake()
        {
            base.Awake();

            this.spriteRenderer = this.GetComponent<SpriteRenderer>();
            this.boxCollider2D = this.GetComponent<BoxCollider2D>();
        }

        #endregion

        #region Methods

        protected override void ApplyNewValue(string newValue)
        {
            base.ApplyNewValue(newValue);
            
            if (string.IsNullOrEmpty(newValue))
            {
                return;
            }

            this.spriteRenderer.sprite = Resources.Load<Sprite>(newValue);

            if (this.boxCollider2D != null)
            {
                this.boxCollider2D.size = this.spriteRenderer.sprite.bounds.size;
            }
        }

        #endregion
    }
}