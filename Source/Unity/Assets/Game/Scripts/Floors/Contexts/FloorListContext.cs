﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FloorListContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Contexts
{
    using System.Linq;

    using DoomedDino.Logic.Physics.Components;
    using DoomedDino.Unity.Floors.Components;
    using DoomedDino.Unity.Floors.Events;
    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Rendering.Components;

    using EZData;

    using Slash.ECS.Events;

    using UnityEngine;

    public class FloorListContext : GameContext
    {
        #region Fields

        private readonly Collection<FloorContext> floors = new Collection<FloorContext>(false);

        #endregion

        #region Constructors and Destructors

        public FloorListContext()
        {
            this.RegisterListener(FloorEvent.FloorCreated, this.OnFloorCreated);
            this.RegisterListener(FloorEvent.FloorRemoved, this.OnFloorRemoved);
        }

        #endregion

        #region Public Properties

        public Collection<FloorContext> Floors
        {
            get
            {
                return this.floors;
            }
        }

        #endregion

        #region Methods

        private void OnFloorBecameInvisible(FloorContext floor)
        {
            if (this.Game != null)
            {
                this.Game.EventManager.QueueEvent(FloorEvent.FloorBecameInvisible, floor.EntityId);
            }
        }

        private void OnFloorBecameVisible(FloorContext floor)
        {
            if (this.Game != null)
            {
                this.Game.EventManager.QueueEvent(FloorEvent.FloorBecameVisible, floor.EntityId);
            }
        }

        private void OnFloorCreated(GameEvent e)
        {
            var entityId = (int)e.EventData;

            // Create floor context.
            var positionComponent = this.Game.EntityManager.GetComponent<PositionComponent>(entityId);
            var spriteComponent = this.Game.EntityManager.GetComponent<SpriteComponent>(entityId);
            var floorComponent = this.Game.EntityManager.GetComponent<FloorComponent>(entityId);

            var floor = new FloorContext();
            this.floors.Add(floor);

            floor.EntityId = entityId;
            floor.Position = new Vector3(positionComponent.Position.X, positionComponent.Position.Y, 0);
            floor.SpriteName = spriteComponent.SpriteName;
            floor.BaseLineHeight = floorComponent.BaseLineHeight;

            floor.FloorBecameVisible += this.OnFloorBecameVisible;
            floor.FloorBecameInvisible += this.OnFloorBecameInvisible;
        }

        private void OnFloorRemoved(GameEvent e)
        {
            var entityId = (int)e.EventData;
            var floor = this.floors.Items.FirstOrDefault(f => f.EntityId == entityId);

            if (floor != null)
            {
                // Remove floor context.
                floor.FloorBecameVisible -= this.OnFloorBecameVisible;
                floor.FloorBecameInvisible -= this.OnFloorBecameInvisible;

                this.floors.Remove(floor);
            }
        }

        #endregion
    }
}