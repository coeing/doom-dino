﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FloorContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Contexts
{
    using System;

    using EZData;

    using UnityEngine;

    public class FloorContext : Context
    {
        #region Fields

        private readonly Property<float> baseLineHeightProperty = new Property<float>();

        private readonly Property<Vector3> positionProperty = new Property<Vector3>();

        private readonly Property<string> spriteNameProperty = new Property<string>();

        #endregion

        #region Public Events

        public event Action<FloorContext> FloorBecameInvisible;

        public event Action<FloorContext> FloorBecameVisible;

        #endregion

        #region Public Properties

        public float BaseLineHeight
        {
            get
            {
                return this.baseLineHeightProperty.GetValue();
            }
            set
            {
                this.baseLineHeightProperty.SetValue(value);
            }
        }

        public Property<float> BaseLineHeightProperty
        {
            get
            {
                return this.baseLineHeightProperty;
            }
        }

        public int EntityId { get; set; }

        public Vector3 Position
        {
            get
            {
                return this.positionProperty.GetValue();
            }
            set
            {
                this.positionProperty.SetValue(value);
            }
        }

        public Property<Vector3> PositionProperty
        {
            get
            {
                return this.positionProperty;
            }
        }

        public string SpriteName
        {
            get
            {
                return this.spriteNameProperty.GetValue();
            }
            set
            {
                this.spriteNameProperty.SetValue(value);
            }
        }

        public Property<string> SpriteNameProperty
        {
            get
            {
                return this.spriteNameProperty;
            }
        }

        #endregion

        #region Public Methods and Operators

        public void OnFloorBecameInvisible()
        {
            var handler = this.FloorBecameInvisible;
            if (handler != null)
            {
                handler(this);
            }
        }

        public void OnFloorBecameVisible()
        {
            var handler = this.FloorBecameVisible;
            if (handler != null)
            {
                handler(this);
            }
        }

        #endregion
    }
}