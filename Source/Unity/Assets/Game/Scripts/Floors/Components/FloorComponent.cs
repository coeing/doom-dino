﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FloorComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class FloorComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Height of the line the character runs on, from the bottom of the sprite.
        /// </summary>
        public const string AttributeBaseLineHeight = "FloorComponent.BaseLineHeight";

        /// <summary>
        ///   Attribute default: Height of the line the character runs on, from the bottom of the sprite.
        /// </summary>
        public const float DefaultBaseLineHeight = 0.0f;

        #endregion

        #region Constructors and Destructors

        public FloorComponent()
        {
            this.BaseLineHeight = DefaultBaseLineHeight;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Height of the line the character runs on, from the bottom of the sprite.
        /// </summary>
        [InspectorFloat(AttributeBaseLineHeight, Default = DefaultBaseLineHeight,
            Description = "Height of the line the character runs on, from the bottom of the sprite.")]
        public float BaseLineHeight { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.BaseLineHeight = attributeTable.GetFloatOrDefault(AttributeBaseLineHeight, DefaultBaseLineHeight);
        }

        #endregion
    }
}