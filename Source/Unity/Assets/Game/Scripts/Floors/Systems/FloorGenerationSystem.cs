﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FloorGenerationSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DoomedDino.Logic.Physics.Components;
    using DoomedDino.Unity.Floors.Components;
    using DoomedDino.Unity.Floors.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Blueprints;
    using Slash.ECS.Events;
    using Slash.ECS.Inspector.Attributes;
    using Slash.ECS.Systems;
    using Slash.Math.Algebra.Vectors;

    [InspectorType]
    [GameSystem]
    public class FloorGenerationSystem : GameSystem
    {
        #region Constants

        /// <summary>
        ///   Attribute: Horizontal overlapping of adjacent floors, in meters.
        /// </summary>
        public const string AttributeHorizontalOverlap = "FloorGenerationSystem.HorizontalOverlap";

        /// <summary>
        ///   Attribute default: Horizontal overlapping of adjacent floors, in meters.
        /// </summary>
        public const float DefaultHorizontalOverlap = 0.0f;

        #endregion

        #region Fields

        private readonly List<Blueprint> floorBlueprints = new List<Blueprint>();

        private readonly Random random = new Random();

        private int lastFloorEntity = -1;

        private float oldFloorWidth;

        private float oldFloorX;

        #endregion

        #region Constructors and Destructors

        public FloorGenerationSystem()
        {
            this.HorizontalOverlap = DefaultHorizontalOverlap;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Horizontal overlapping of adjacent floors, in meters.
        /// </summary>
        [InspectorFloat(AttributeHorizontalOverlap, Default = DefaultHorizontalOverlap,
            Description = "Horizontal overlapping of adjacent floors, in meters.")]
        public float HorizontalOverlap { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.HorizontalOverlap = configuration.GetFloatOrDefault(
                AttributeHorizontalOverlap, DefaultHorizontalOverlap);

            this.Game.EventManager.RegisterListener(FloorEvent.FloorBecameVisible, this.OnFloorBecameVisible);
            this.Game.EventManager.RegisterListener(FloorEvent.FloorBecameInvisible, this.OnFloorBecameInvisible);

            // Get floor blueprints.
            foreach (var blueprint in
                this.Game.BlueprintManager.Where(blueprint => blueprint.ComponentTypes.Contains(typeof(FloorComponent)))
                )
            {
                this.floorBlueprints.Add(blueprint);
            }

            // Generate initial floors.
            this.GenerateInitialFloors();
        }

        #endregion

        #region Methods

        private void AddFloor()
        {
            // Get next random floor.
            var floorIndex = this.random.Next(this.floorBlueprints.Count);
            var floorBlueprint = this.floorBlueprints[floorIndex];

            // Create floor entity.
            this.lastFloorEntity = this.Game.EntityManager.CreateEntity(floorBlueprint);

            // Get new floor width.
            var boundsComponent = this.Game.EntityManager.GetComponent<BoundsComponent>(this.lastFloorEntity);
            var newFloorWidth = boundsComponent.Width;

            // Set new floor position.
            var positionComponent = this.Game.EntityManager.GetComponent<PositionComponent>(this.lastFloorEntity);
            var floorComponent = this.Game.EntityManager.GetComponent<FloorComponent>(this.lastFloorEntity);

            var newFloorX = this.oldFloorX + (this.oldFloorWidth + newFloorWidth) / 2 - this.HorizontalOverlap;
            var newFloorY = boundsComponent.Height / 2 + floorComponent.BaseLineHeight;
            positionComponent.Position = new Vector2F(newFloorX, -newFloorY);

            // Update level generation context.
            this.oldFloorWidth = newFloorWidth;
            this.oldFloorX = newFloorX;

            // Notify listeners.
            this.Game.EventManager.QueueEvent(FloorEvent.FloorCreated, this.lastFloorEntity);
        }

        private void GenerateInitialFloors()
        {
            while (this.oldFloorX < 20)
            {
                this.AddFloor();
            }
        }

        private void OnFloorBecameInvisible(GameEvent e)
        {
            var entityId = (int)e.EventData;
            this.Game.EntityManager.RemoveEntity(entityId);
            this.Game.EventManager.QueueEvent(FloorEvent.FloorRemoved, entityId);
        }

        private void OnFloorBecameVisible(GameEvent e)
        {
            var entityId = (int)e.EventData;

            if (entityId == this.lastFloorEntity)
            {
                // Last floor became visible - create new one.
                this.AddFloor();
            }
        }

        #endregion
    }
}