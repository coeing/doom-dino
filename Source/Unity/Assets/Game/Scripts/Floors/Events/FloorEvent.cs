﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FloorEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Floors.Events
{
    public enum FloorEvent
    {
        FloorCreated,

        FloorBecameVisible,

        FloorBecameInvisible,

        FloorRemoved
    }
}