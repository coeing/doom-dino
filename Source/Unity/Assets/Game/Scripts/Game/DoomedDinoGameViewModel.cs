﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToyHuntGameViewModel.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Game
{
    using UnityEngine;

    public class DoomedDinoGameViewModel : MonoBehaviour
    {
        #region Fields

        public DoomedDinoGameContext Context;

        public NguiRootContext View;

        #endregion

        #region Methods

        private void Awake()
        {
            this.Context = new DoomedDinoGameContext();
            this.View.SetContext(this.Context);
        }

        private void Update()
        {
            this.Context.Update(Time.deltaTime);
        }

        #endregion
    }
}