﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RunnerBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Game.Bindings
{
    using DoomedDino.Unity.Game.Contexts;

    using Slash.Unity.NDataExt.Bindings;

    public class RunnerBinding : ChildPrefabBinding<RunnerContext>
    {
    }
}