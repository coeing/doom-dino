﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControllerCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Game.Bindings
{
    using Slash.Unity.NDataExt.Bindings;

    using UnityEngine;

#if !UNITY_WEBPLAYER
    using XInputDotNetPure;
#endif

    public class ControllerCommand<T> : CommandBinding<T>
    {
        #region Fields

        public GamePadButtons GamePadButton;

#if !UNITY_WEBPLAYER      
        public PlayerIndex GamePadIndex;
#else
        public int GamePadIndex;
#endif

        public string InputName;

        private bool buttonUsed;

        #endregion

        #region Public Properties

        public bool Pressed
        {
            get
            {
                if (!string.IsNullOrEmpty(this.InputName) && Input.GetButtonDown(this.InputName))
                {
                    return true;
                }

#if !UNITY_WEBPLAYER 
                var buttonState = this.GetButtonState();
                if (buttonState == ButtonState.Pressed)
                {
                    return true;
                }
#endif

                return false;
            }
        }

        #endregion

        #region Properties

#if !UNITY_WEBPLAYER 
        private GamePadState GamePadState
        {
            get
            {
                return GamePad.GetState(this.GamePadIndex);
            }
        }
#endif

        #endregion

        #region Methods

        protected virtual void OnButtonPress()
        {
        }

#if !UNITY_WEBPLAYER 
        private ButtonState GetButtonState()
        {
            switch (this.GamePadButton)
            {
                case GamePadButtons.A:
                    return this.GamePadState.Buttons.A;
                case GamePadButtons.B:
                    return this.GamePadState.Buttons.B;
                case GamePadButtons.X:
                    return this.GamePadState.Buttons.X;
                case GamePadButtons.Y:
                    return this.GamePadState.Buttons.Y;
                case GamePadButtons.Start:
                    return this.GamePadState.Buttons.Start;
                case GamePadButtons.Back:
                    return this.GamePadState.Buttons.Back;
            }
            return ButtonState.Released;
        }
#endif

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            if (this.buttonUsed && !this.Pressed)
            {
                this.buttonUsed = false;
            }

            if (!this.buttonUsed && this.Pressed)
            {
                this.OnButtonPress();
                this.buttonUsed = true;
            }
        }

        #endregion
    }

    public enum GamePadButtons
    {
        None,

        A,

        B,

        X,

        Y,

        Start,

        Back,
    }
}