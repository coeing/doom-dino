﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Game.Systems
{
    using DoomedDino.Unity.Game.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Blueprints;
    using Slash.ECS.Events;
    using Slash.ECS.Inspector.Attributes;
    using Slash.ECS.Systems;

    [InspectorType]
    [GameSystem]
    public class PlayerSystem : GameSystem
    {
        #region Constants

        /// <summary>
        ///   Attribute: Runner blueprint
        /// </summary>
        public const string AttributeRunnerBlueprint = "PlayerSystem.RunnerBlueprint";

        /// <summary>
        ///   Attribute default: Runner blueprint
        /// </summary>
        public const Blueprint DefaultRunnerBlueprint = null;

        #endregion

        #region Fields

        private Blueprint runnerBlueprint;

        private int runnerEntityId;

        #endregion

        #region Public Properties

        /// <summary>
        ///   Runner blueprint
        /// </summary>
        [InspectorBlueprint(AttributeRunnerBlueprint, Description = "Runner blueprint", Default = DefaultRunnerBlueprint
            )]
        public string RunnerBlueprint { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.runnerBlueprint = this.Game.BlueprintManager.GetBlueprint(this.RunnerBlueprint);
            this.Game.EventManager.RegisterListener(FrameworkEvent.GameStarted, this.OnGameStarted);
            this.Game.EventManager.RegisterListener(DoomedDinoEvent.GameFinished, this.OnGameFinished);
        }

        #endregion

        #region Methods

        private void OnGameFinished(GameEvent e)
        {
            // Remove runner.
            this.Game.EntityManager.RemoveEntity(this.runnerEntityId);
            this.Game.EventManager.QueueEvent(
                DoomedDinoEvent.RunnerRemoved, new EntityEventData { EntityId = this.runnerEntityId });
        }

        private void OnGameStarted(GameEvent e)
        {
            // Create runner.
            AttributeTable configuration = new AttributeTable();
            this.runnerEntityId = this.Game.EntityManager.CreateEntity(this.runnerBlueprint, configuration);

            this.Game.EventManager.QueueEvent(
                DoomedDinoEvent.RunnerInstantiated, new EntityEventData { EntityId = this.runnerEntityId });
        }

        #endregion
    }
}