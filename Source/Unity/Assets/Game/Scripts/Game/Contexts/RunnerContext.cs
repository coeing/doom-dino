﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RunnerContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Game.Contexts
{
    using DoomedDino.Logic.Physics.Components;
    using DoomedDino.Unity.Movement.Contexts;

    using EZData;

    using Slash.Math.Algebra.Vectors;

    using UnityEngine;

    public class RunnerContext : GameContext
    {
        #region Fields

        private readonly Property<Vector3> positionProperty = new Property<Vector3>();

        #endregion

        #region Constructors and Destructors

        public RunnerContext()
        {
            this.Movement = new CharacterMovementContext();
        }

        #endregion

        #region Public Properties

        public CharacterMovementContext Movement { get; set; }

        public Vector3 Position
        {
            get
            {
                return this.positionProperty.GetValue();
            }
            set
            {
                this.positionProperty.SetValue(value);
            }
        }

        public Property<Vector3> PositionProperty
        {
            get
            {
                return this.positionProperty;
            }
        }

        #endregion

        #region Public Methods and Operators

        public void Init(int entityId)
        {
            this.Position =
                this.ConvertPosition(this.Game.EntityManager.GetComponent<PositionComponent>(entityId).Position);
            Debug.Log("Initial position" + this.Position);
        }

        #endregion

        #region Methods

        private Vector3 ConvertPosition(Vector2F position)
        {
            return new Vector3(position.X, position.Y, 0);
        }

        #endregion
    }
}