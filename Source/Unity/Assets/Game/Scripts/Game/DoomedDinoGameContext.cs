﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DoomedDinoGameContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Game
{
    using DoomedDino.Unity.Doom.Contexts;
    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Floors.Contexts;
    using DoomedDino.Unity.Game.Contexts;
    using DoomedDino.Unity.Game.Events;
    using DoomedDino.Unity.Obstacles.Contexts;
    using DoomedDino.Unity.Score.Contexts;

    using EZData;

    using Slash.ECS.Events;
    using Slash.Unity.Common.Scenes;

    using UnityEngine;

    public class DoomedDinoGameContext : GameContext
    {
        #region Constants

        private const float gameOverDelay = 2.0f;

        #endregion

        #region Fields

        public float FollowRunnerDelay = 1.0f;

        private readonly Property<bool> followRunnerProperty = new Property<bool>();

        private readonly Property<bool> isGameOverProperty = new Property<bool>();

        private readonly Property<bool> isRunningProperty = new Property<bool>();

        private readonly Property<RunnerContext> runnerProperty = new Property<RunnerContext>();

        private float gameOverTimeRemaining = 2.0f;

        private float remainingFollowRunnerDelay;

        private bool waitingForGameOver;

        #endregion

        #region Constructors and Destructors

        public DoomedDinoGameContext()
        {
            this.FloorList = new FloorListContext();
            this.Obstacles = new ObstaclesContext();
            this.Score = new ScoreContext();
            this.Doom = new DoomContext();
            this.Saboteur = new SaboteurContext();
            this.Comets = new CometsContext();

            this.RegisterListener(DoomedDinoEvent.RunnerInstantiated, this.OnRunnerInstantiated);
            this.RegisterListener(DoomedDinoEvent.RunnerRemoved, this.OnRunnerRemoved);
            this.RegisterListener(FrameworkEvent.GameStarted, this.OnGameStarted);
            this.RegisterListener(DoomEvent.ArmageddonStart, this.OnGameFinished);
            this.RegisterListener(DoomedDinoEvent.GameFinished, this.OnGameFinished);
        }

        #endregion

        #region Public Properties

        public CometsContext Comets { get; set; }

        public DoomContext Doom { get; set; }

        public FloorListContext FloorList { get; set; }

        public bool FollowRunner
        {
            get
            {
                return this.followRunnerProperty.GetValue();
            }
            set
            {
                this.followRunnerProperty.SetValue(value);
            }
        }

        public Property<bool> FollowRunnerProperty
        {
            get
            {
                return this.followRunnerProperty;
            }
        }

        public bool IsGameOver
        {
            get
            {
                return this.isGameOverProperty.GetValue();
            }
            set
            {
                this.isGameOverProperty.SetValue(value);
            }
        }

        public Property<bool> IsGameOverProperty
        {
            get
            {
                return this.isGameOverProperty;
            }
        }

        public bool IsRunning
        {
            get
            {
                return this.isRunningProperty.GetValue();
            }
            set
            {
                this.isRunningProperty.SetValue(value);
            }
        }

        public Property<bool> IsRunningProperty
        {
            get
            {
                return this.isRunningProperty;
            }
        }

        public ObstaclesContext Obstacles { get; set; }

        public RunnerContext Runner
        {
            get
            {
                return this.runnerProperty.GetValue();
            }
            set
            {
                this.runnerProperty.SetValue(value);
            }
        }

        public Property<RunnerContext> RunnerProperty
        {
            get
            {
                return this.runnerProperty;
            }
        }

        public SaboteurContext Saboteur { get; set; }

        public ScoreContext Score { get; set; }

        #endregion

        #region Public Methods and Operators

        public void OnBackToMainMenu()
        {
            SceneManager.Instance.ChangeScene("MainMenuScene");
        }

        public void OnRestartGame()
        {
            if (!this.IsRunning)
            {
                SceneManager.Instance.ChangeScene(Application.loadedLevelName);
            }
        }

        public void Update(float dt)
        {
            if (this.IsRunning)
            {
                if (this.remainingFollowRunnerDelay > 0)
                {
                    this.remainingFollowRunnerDelay -= dt;
                    if (this.remainingFollowRunnerDelay <= 0)
                    {
                        this.FollowRunner = true;
                    }
                }
            }

            if (this.waitingForGameOver)
            {
                this.gameOverTimeRemaining -= dt;

                if (this.gameOverTimeRemaining <= 0)
                {
                    this.IsGameOver = true;
                }
            }
        }

        #endregion

        #region Methods

        private void OnGameFinished(GameEvent e)
        {
            this.IsRunning = false;
            this.FollowRunner = false;
            this.waitingForGameOver = true;
        }

        private void OnGameStarted(GameEvent e)
        {
            this.IsRunning = true;
            this.remainingFollowRunnerDelay = this.FollowRunnerDelay;
            this.IsGameOver = false;
            this.gameOverTimeRemaining = gameOverDelay;
        }

        private void OnRunnerInstantiated(GameEvent e)
        {
            EntityEventData data = (EntityEventData)e.EventData;
            this.Runner = new RunnerContext();
            this.Runner.Init(data.EntityId);
        }

        private void OnRunnerRemoved(GameEvent e)
        {
            this.Runner = null;
        }

        #endregion
    }
}