﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToyHuntGameBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Game
{
    using Slash.Unity.Common.Configuration;
    using Slash.Unity.Common.ECS;
    using Slash.Unity.Common.Scenes;

    using UnityEngine;

    public class DoomedDinoGameBehaviour : MonoBehaviour
    {
        #region Fields

        private GameBehaviour gameBehaviour;

        private SceneManager sceneManager;

        #endregion

        #region Public Methods and Operators

        public void StartNewGame()
        {
            var game = new Slash.ECS.Game();
            game.BlueprintManager = FindObjectOfType<GameConfigurationBehaviour>().BlueprintManager;
            this.gameBehaviour.StartGame(game);
        }

        #endregion

        private void Awake()
        {
            this.sceneManager = SceneManager.Instance;
            this.gameBehaviour = this.GetComponent<GameBehaviour>();
        }

        private void Start()
        {
            this.StartNewGame();
        }

    }
}