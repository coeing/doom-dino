﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DoomedDinoEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace DoomedDino.Unity.Game.Events
{
    public enum DoomedDinoEvent

    {
        RunnerInstantiated,

        GameFinished,

        RunnerRemoved
    }
}