﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpriteAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Animation
{
    using System;

    using UnityEngine;

    public class SpriteAnimation : MonoBehaviour
    {
        #region Fields

        public SpriteAnimationData Animation;

        public UI2DSprite NGUISprite;

        public float SpeedFactor;

        public SpriteRenderer SpriteRenderer;

        private int frameIndex;

        private float timeElapsed;

        #endregion

        #region Public Events

        public event Action<string> AnimationFinished;

        #endregion

        #region Public Methods and Operators

        public void PlayAnimation(SpriteAnimationData anim)
        {
            this.PlayAnimation(anim, 1.0f);
        }

        public void PlayAnimation(SpriteAnimationData anim, float animSpeedFactor)
        {
            if (this.Animation != anim)
            {
                this.Animation = anim;
                this.timeElapsed = 0f;
                this.frameIndex = 0;

                // Show new first sprite immediately.
                if (this.SpriteRenderer != null)
                {
                    this.SpriteRenderer.sprite = this.Animation.Sprites[0];
                }
                if (this.NGUISprite != null)
                {
                    this.NGUISprite.sprite2D = this.Animation.Sprites[0];
                }
            }

            this.SpeedFactor = animSpeedFactor;
        }

        #endregion

        #region Methods

        private void OnAnimationFinished(string animationName)
        {
            var handler = this.AnimationFinished;
            if (handler != null)
            {
                handler(animationName);
            }
        }

        private void Update()
        {
            if (this.Animation == null)
            {
                return;
            }

            this.timeElapsed += Time.deltaTime * this.SpeedFactor;

            float frameDuration = this.Animation.FrameDuration;
            if (this.timeElapsed > frameDuration)
            {
                this.timeElapsed -= frameDuration;
                this.frameIndex = (this.frameIndex + 1) % this.Animation.Sprites.Length;

                if (this.frameIndex == 0)
                {
                    // Dispatch event.
                    this.OnAnimationFinished(this.Animation.name);

                    // Replace with next animation.
                    if (this.Animation.Next != null)
                    {
                        this.PlayAnimation(this.Animation.Next, this.SpeedFactor);
                    }
                    else if (!this.Animation.Loop)
                    {
                        this.Animation = null;
                    }
                }

                if (this.Animation != null)
                {
                    if (this.SpriteRenderer != null)
                    {
                        this.SpriteRenderer.sprite = this.Animation.Sprites[this.frameIndex];
                    }
                    if (this.NGUISprite != null)
                    {
                        this.NGUISprite.sprite2D = this.Animation.Sprites[this.frameIndex];
                    }
                }
            }
        }

        #endregion
    }
}