﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Animation
{
    using System.Collections;

    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Movement.Events;

    using Slash.ECS.Events;
    using Slash.Unity.Common.ECS;

    using UnityEngine;

    public class CharacterAnimation : GameEventBehaviour
    {
        #region Fields

        public SpriteAnimationData DashAnimation;

        public SpriteAnimationData HitAnimation;

        public SpriteAnimationData JumpAnimation;

        public SpriteAnimation SpriteAnimation;

        public SpriteAnimationData WalkAnimation;

        private bool isDashing;

        private bool isHit;

        private bool isJumping;

        #endregion

        #region Methods

        protected override void RegisterListeners()
        {
            this.RegisterListener(MovementEvent.JumpStarted, this.OnJumpStarted);
            this.RegisterListener(MovementEvent.JumpEnded, this.OnJumpEnded);
            this.RegisterListener(MovementEvent.DashStarted, this.OnDashStarted);
            this.RegisterListener(MovementEvent.DashEnded, this.OnDashEnded);
            this.RegisterListener(DoomEvent.CurrentDoomChanged, this.OnDoomChanged);
        }

        private void OnDashEnded(GameEvent e)
        {
            this.isDashing = false;
            this.UpdateAnimation();
        }

        private void OnDashStarted(GameEvent e)
        {
            this.isDashing = true;
            this.UpdateAnimation();
        }

        private void OnDoomChanged(GameEvent e)
        {
            this.StartCoroutine(this.PlayHit());
        }

        private void OnJumpEnded(GameEvent e)
        {
            this.isJumping = false;
            this.UpdateAnimation();
        }

        private void OnJumpStarted(GameEvent e)
        {
            this.isJumping = true;
            this.UpdateAnimation();
        }

        private IEnumerator PlayHit()
        {
            this.isHit = true;
            this.UpdateAnimation();

            yield return new WaitForSeconds(0.5f);

            this.isHit = false;
            this.UpdateAnimation();
        }

        private void UpdateAnimation()
        {
            SpriteAnimationData activeAnimation;
            if (this.isHit)
            {
                activeAnimation = this.HitAnimation;
            }
            else if (this.isDashing)
            {
                activeAnimation = this.DashAnimation;
            }
            else if (this.isJumping)
            {
                activeAnimation = this.JumpAnimation;
            }
            else
            {
                activeAnimation = this.WalkAnimation;
            }
            this.SpriteAnimation.PlayAnimation(activeAnimation);
        }

        #endregion
    }
}