﻿namespace DoomedDino.Unity.Animation
{
    using System.Linq;

    using DoomedDino.Unity.Movement.Events;

    using Slash.ECS.Events;
    using Slash.Unity.Common.ECS;

    public class BlockAnimation : GameEventBehaviour
    {
        #region Fields

        public SpriteAnimationData[] Animations;

        public SpriteAnimation SpriteAnimation;

        #endregion

        #region Public Methods and Operators

        public SpriteAnimationData GetAnimation(string animationName)
        {
            return this.Animations.FirstOrDefault(anim => anim.gameObject.name.StartsWith(animationName));
        }
        #endregion

        #region Methods

        protected override void RegisterListeners()
        {
            this.RegisterListener(MovementEvent.JumpStarted, this.OnJumpStarted);
            this.RegisterListener(MovementEvent.JumpEnded, this.OnJumpEnded);
            this.RegisterListener(MovementEvent.DashStarted, this.OnDashStarted);
            this.RegisterListener(MovementEvent.DashEnded, this.OnDashEnded);
        }

        private void OnDashEnded(GameEvent e)
        {
            var newAnimation = this.GetAnimation("dino_walk");
            this.SpriteAnimation.PlayAnimation(newAnimation);
        }

        private void OnDashStarted(GameEvent e)
        {
            var newAnimation = this.GetAnimation("dino_dash");
            this.SpriteAnimation.PlayAnimation(newAnimation);
        }

        private void OnJumpEnded(GameEvent e)
        {
            var newAnimation = this.GetAnimation("dino_walk");
            this.SpriteAnimation.PlayAnimation(newAnimation);
        }

        private void OnJumpStarted(GameEvent e)
        {
            var newAnimation = this.GetAnimation("dino_jump");
            this.SpriteAnimation.PlayAnimation(newAnimation);
        }

        #endregion
    }
}
