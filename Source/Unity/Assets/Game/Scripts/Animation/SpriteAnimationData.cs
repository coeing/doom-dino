﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpriteAnimationData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Animation
{
    using UnityEngine;

    public class SpriteAnimationData : MonoBehaviour
    {
        #region Fields

        public float FrameDuration;

        /// <summary>
        ///   Indicates if animation is a loop animation.
        /// </summary>
        public bool Loop = true;

        /// <summary>
        ///   Following animation data after finished.
        /// </summary>
        public SpriteAnimationData Next;

        public Sprite[] Sprites;

        #endregion
    }
}