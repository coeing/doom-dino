﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DestroyBlockAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Animation
{
    using DoomedDino.Unity.Obstacles.Events;

    using Slash.ECS.Events;
    using Slash.Unity.Common.ECS;
    using Slash.Unity.NDataExt.Bindings;

    public class DestroyBlockAnimation : GameEventBehaviour
    {
        #region Fields

        public SpriteAnimationData DestroyAnimation;

        public SimpleIntBinding EntityId;

        public SpriteAnimation SpriteAnimation;

        #endregion

        #region Methods

        protected override void RegisterListeners()
        {
            this.RegisterListener(ObstacleEvent.BlockCollision, this.OnBlockCollision);
        }

        private void OnBlockCollision(GameEvent e)
        {
            if (this.SpriteAnimation == null || this.EntityId == null || this.DestroyAnimation == null)
            {
                return;
            }

            int blockEntityId = (int)e.EventData;

            // Check if this block.
            if (this.EntityId != null && blockEntityId == this.EntityId.Value)
            {
                this.SpriteAnimation.PlayAnimation(this.DestroyAnimation);
            }
        }

        #endregion
    }
}