﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScoreEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace DoomedDino.Unity.Score.Events
{
    public enum ScoreEvent
    {
        CurrentScoreChanged
    }
}