﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScoreContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Score.Contexts
{
    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Score.Events;

    using EZData;

    using Slash.ECS.Events;

    public class ScoreContext : GameContext
    {
        #region Fields

        private readonly Property<int> currentScoreProperty = new Property<int>();

        #endregion

        #region Constructors and Destructors

        public ScoreContext()
        {
            this.RegisterListener(ScoreEvent.CurrentScoreChanged, this.OnCurrentScoreChanged);
        }

        #endregion

        #region Public Properties

        public int CurrentScore
        {
            get
            {
                return this.currentScoreProperty.GetValue();
            }
            set
            {
                this.currentScoreProperty.SetValue(value);
            }
        }

        public Property<int> CurrentScoreProperty
        {
            get
            {
                return this.currentScoreProperty;
            }
        }

        #endregion

        #region Methods

        private void OnCurrentScoreChanged(GameEvent e)
        {
            this.CurrentScore = (int)e.EventData;
        }

        #endregion
    }
}