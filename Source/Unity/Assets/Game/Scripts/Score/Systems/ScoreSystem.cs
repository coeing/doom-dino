﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScoreSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Score.Systems
{
    using DoomedDino.Unity.Floors.Events;
    using DoomedDino.Unity.Score.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Inspector.Attributes;
    using Slash.ECS.Systems;

    [InspectorType]
    [GameSystem]
    public class ScoreSystem : GameSystem
    {
        #region Constants

        /// <summary>
        ///   Attribute: Score granted for each floor tile completed.
        /// </summary>
        public const string AttributeScorePerFloor = "ScoreSystem.ScorePerFloor";

        /// <summary>
        ///   Attribute default: Score granted for each floor tile completed.
        /// </summary>
        public const int DefaultScorePerFloor = 10;

        #endregion

        #region Fields

        private int currentScore;

        #endregion

        #region Public Properties

        /// <summary>
        ///   Score granted for each floor tile completed.
        /// </summary>
        [InspectorInt(AttributeScorePerFloor, Default = DefaultScorePerFloor,
            Description = "Score granted for each floor tile completed.")]
        public int ScorePerFloor { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(FloorEvent.FloorBecameVisible, this.OnFloorBecameVisible);
        }

        #endregion

        #region Methods

        private void OnFloorBecameVisible(GameEvent e)
        {
            this.currentScore += this.ScorePerFloor;
            this.Game.EventManager.QueueEvent(ScoreEvent.CurrentScoreChanged, this.currentScore);
        }

        #endregion
    }
}