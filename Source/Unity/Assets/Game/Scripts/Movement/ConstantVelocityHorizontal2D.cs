﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstantVelocity2D.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Movement
{
    using UnityEngine;

    public class ConstantVelocityHorizontal2D : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Constant velocity to use (in m/s).
        /// </summary>
        public float Velocity;

        #endregion

        #region Methods

        private void FixedUpdate()
        {
            if (this.rigidbody2D != null)
            {
                this.rigidbody2D.velocity = new Vector2(this.Velocity, this.rigidbody2D.velocity.y);
            }
        }

        #endregion
    }
}