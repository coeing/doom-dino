﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterMovementContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace DoomedDino.Unity.Movement.Contexts
{
    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Movement.Events;

    public class CharacterMovementContext : GameContext
    {
         public void OnDash()
         {
             this.Game.EventManager.QueueEvent(MovementAction.Dash);
         }
    }
}