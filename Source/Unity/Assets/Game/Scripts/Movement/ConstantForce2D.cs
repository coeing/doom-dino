﻿namespace DoomedDino.Unity.Movement
{
    using UnityEngine;

    public class ConstantForce2D : MonoBehaviour
    {
        #region Methods

        public Vector2 Force;

        private void Update()
        {
            if (this.rigidbody2D != null)
            {
                this.rigidbody2D.AddForce(this.Force, ForceMode2D.Force);
            }
        }

        #endregion
    }
}
