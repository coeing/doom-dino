﻿namespace DoomedDino.Unity.Movement
{
    using UnityEngine;

    public class GroundCheck : MonoBehaviour
    {
        #region Methods

        public bool IsGrounded;

        public float GroundedRadius;

        private void FixedUpdate()
        {
            this.IsGrounded = Physics2D.OverlapCircle(this.transform.position, GroundedRadius, GroundLayerMask) != null;
        }

        public LayerMask GroundLayerMask;

        #endregion
    }
}
