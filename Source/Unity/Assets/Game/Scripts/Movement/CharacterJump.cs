﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterJump.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Movement
{
    using DoomedDino.Unity.Input;
    using DoomedDino.Unity.Movement.Events;

    using Slash.Unity.Common.ECS;

    using UnityEngine;

    public class CharacterJump : GameEventBehaviour
    {
        #region Fields

        public CharacterInput CharacterInput;

        public bool DoubleJumpAvailable;

        public GroundCheck GroundCheck;

        public Vector2 JumpForce;

        private bool buttonUsed;

        private bool wasGrounded;

        #endregion

        #region Public Methods and Operators

        public void OnJump()
        {
            if (this.rigidbody2D != null)
            {
                this.rigidbody2D.AddForce(this.JumpForce, ForceMode2D.Impulse);
                this.Game.EventManager.QueueEvent(MovementEvent.JumpStarted);
            }
        }

        #endregion

        #region Methods

        private void Update()
        {
            bool isGrounded = this.GroundCheck == null || this.GroundCheck.IsGrounded;
            if (isGrounded && !this.wasGrounded)
            {
                this.Game.EventManager.QueueEvent(MovementEvent.JumpEnded);
            }

            if (isGrounded)
            {
                this.DoubleJumpAvailable = true;
            }

            if (!this.CharacterInput.JumpPressed)
            {
                this.buttonUsed = false;
            }

            if (this.CharacterInput.JumpPressed && !this.buttonUsed)
            {
                this.buttonUsed = true;
                if (isGrounded)
                {
                    this.OnJump();
                }
                else if (this.DoubleJumpAvailable)
                {
                    this.OnJump();
                    this.DoubleJumpAvailable = false;
                }
            }

            this.wasGrounded = isGrounded;
        }

        #endregion
    }
}