﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MovementSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Movement.Systems
{
    using DoomedDino.Unity.Movement.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Inspector.Attributes;
    using Slash.ECS.Systems;

    [InspectorType]
    [GameSystem]
    public class MovementSystem : GameSystem
    {
        #region Constants

        /// <summary>
        ///   Attribute: Dash cooldown
        /// </summary>
        public const string AttributeDashCooldown = "MovementSystem.DashCooldown";

        /// <summary>
        ///   Attribute: Dash duration.
        /// </summary>
        public const string AttributeDashDuration = "MovementSystem.DashDuration";

        /// <summary>
        ///   Attribute default: Dash cooldown
        /// </summary>
        public const float DefaultDashCooldown = 1.0f;

        /// <summary>
        ///   Attribute default: Dash duration.
        /// </summary>
        public const float DefaultDashDuration = 1.0f;

        #endregion

        #region Fields

        private float remainingDashCooldown;

        private float remainingDashDuration;

        #endregion

        #region Public Properties

        /// <summary>
        ///   Dash cooldown
        /// </summary>
        [InspectorFloat(AttributeDashCooldown, Description = "Dash cooldown", Default = DefaultDashCooldown)]
        public float DashCooldown { get; set; }

        /// <summary>
        ///   Dash duration.
        /// </summary>
        [InspectorFloat(AttributeDashDuration, Description = "Dash duration.", Default = DefaultDashDuration)]
        public float DashDuration { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(MovementAction.Dash, this.OnDash);
        }

        public override void UpdateSystem(float dt)
        {
            if (this.remainingDashDuration > 0)
            {
                this.remainingDashDuration -= dt;
                if (this.remainingDashDuration <= 0.0f)
                {
                    this.EndDash();
                }
            }
            if (this.remainingDashCooldown > 0)
            {
                this.remainingDashCooldown -= dt;
            }
        }

        #endregion

        #region Methods

        private void EndDash()
        {
            this.Game.EventManager.QueueEvent(MovementEvent.DashEnded);
        }

        private void OnDash(GameEvent e)
        {
            // TODO(co): Check if dash can be started.
            if (this.remainingDashCooldown > 0)
            {
                this.Game.Log.Error("Dash not possible, cooldown not over.");
                return;
            }

            // Start dash.
            this.remainingDashCooldown = this.DashCooldown;
            this.remainingDashDuration = this.DashDuration;

            this.Game.EventManager.QueueEvent(MovementEvent.DashStarted);
        }

        #endregion
    }
}