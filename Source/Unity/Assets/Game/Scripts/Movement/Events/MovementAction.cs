﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MovementAction.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace DoomedDino.Unity.Movement.Events
{
    public enum MovementAction
    {
        Dash,
        Jump,
        Slide,
    }
}