﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MovementEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Movement.Events
{
    public enum MovementEvent
    {
        DashStarted,

        DashEnded,

        JumpStarted,

        JumpEnded
    }
}