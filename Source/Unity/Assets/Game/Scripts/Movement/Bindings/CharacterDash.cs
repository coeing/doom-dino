﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterDash.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Movement.Bindings
{
    using DoomedDino.Unity.Input;

    using EZData;

    using Slash.Unity.NDataExt.Bindings;

    public class CharacterDash : CommandBinding<Command>
    {
        #region Fields

        public CharacterInput CharacterInput;

        public float DashForce = 1000.0f;

        private bool dashUsed;

        #endregion

        #region Methods

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            if (this.dashUsed && !this.CharacterInput.DashPressed)
            {
                this.dashUsed = false;
            }

            if (!this.dashUsed && this.CharacterInput.DashPressed)
            {
                this.InvokeCommand();
                this.dashUsed = true;
            }
        }

        #endregion
    }
}