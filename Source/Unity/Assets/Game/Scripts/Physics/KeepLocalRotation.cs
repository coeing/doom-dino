﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeepLocalRotation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Assets.Game.Scripts.Physics
{
    using UnityEngine;

    public class KeepLocalRotation : MonoBehaviour
    {
        #region Fields

        private Quaternion initialRotation;

        #endregion

        #region Methods

        private void Start()
        {
            this.initialRotation = this.transform.localRotation;
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            this.transform.localRotation = this.initialRotation;
        }

        #endregion
    }
}