﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PositionComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Logic.Physics.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;
    using Slash.Math.Algebra.Vectors;

    [InspectorComponent]
    public class PositionComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Position of this entity.
        /// </summary>
        public const string AttributePosition = "PositionComponent.Position";

        /// <summary>
        ///   Attribute default: Position of this entity.
        /// </summary>
        public const Vector2F DefaultPosition = null;

        #endregion

        #region Constructors and Destructors

        public PositionComponent()
        {
            this.Position = DefaultPosition;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Position of this entity.
        /// </summary>
        [InspectorVector(AttributePosition, Default = DefaultPosition, Description = "Position of this entity.")]
        public Vector2F Position { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.Position = attributeTable.GetValueOrDefault(AttributePosition, DefaultPosition);
        }

        #endregion
    }
}