﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BoundsComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Logic.Physics.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class BoundsComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Height of this entity, in meters.
        /// </summary>
        public const string AttributeHeight = "BoundsComponent.Height";

        /// <summary>
        ///   Attribute: Width of this entity, in meters.
        /// </summary>
        public const string AttributeWidth = "BoundsComponent.Width";

        /// <summary>
        ///   Attribute default: Height of this entity, in meters.
        /// </summary>
        public const float DefaultHeight = 0.0f;

        /// <summary>
        ///   Attribute default: Width of this entity, in meters.
        /// </summary>
        public const float DefaultWidth = 0.0f;

        #endregion

        #region Constructors and Destructors

        public BoundsComponent()
        {
            this.Width = DefaultWidth;
            this.Height = DefaultHeight;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Height of this entity, in meters.
        /// </summary>
        [InspectorFloat(AttributeHeight, Default = DefaultHeight, Description = "Height of this entity, in meters.")]
        public float Height { get; set; }

        /// <summary>
        ///   Width of this entity, in meters.
        /// </summary>
        [InspectorFloat(AttributeWidth, Default = DefaultWidth, Description = "Width of this entity, in meters.")]
        public float Width { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.Width = attributeTable.GetFloatOrDefault(AttributeWidth, DefaultWidth);
            this.Height = attributeTable.GetFloatOrDefault(AttributeHeight, DefaultHeight);
        }

        #endregion
    }
}