﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CometImpactEffectComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Logic.Obstacles.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class CometImpactEffectComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Blueprint id of the obstacle to create.
        /// </summary>
        public const string AttributeObstacleBlueprintId = "CometImpactEffectComponent.ObstacleBlueprintId";

        /// <summary>
        ///   Attribute default: Blueprint id of the obstacle to create.
        /// </summary>
        public const string DefaultObstacleBlueprintId = "";

        #endregion

        #region Constructors and Destructors

        public CometImpactEffectComponent()
        {
            this.ObstacleBlueprintId = DefaultObstacleBlueprintId;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Blueprint id of the obstacle to create.
        /// </summary>
        [InspectorString(AttributeObstacleBlueprintId, Default = DefaultObstacleBlueprintId,
            Description = "Blueprint id of the obstacle to create.")]
        public string ObstacleBlueprintId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.ObstacleBlueprintId = attributeTable.GetValueOrDefault(
                AttributeObstacleBlueprintId, DefaultObstacleBlueprintId);
        }

        #endregion
    }
}