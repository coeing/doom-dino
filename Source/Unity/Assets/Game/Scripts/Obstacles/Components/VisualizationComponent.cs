﻿namespace DoomedDino.Logic.Obstacles.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class VisualizationComponent : IEntityComponent
    {
        /// <summary>
        ///   Attribute: Visualization.
        /// </summary>
        public const string AttributeVisualization = "VisualizationComponent.Visualization";

        /// <summary>
        ///   Attribute default: Visualization.
        /// </summary>
        public const string DefaultVisualization = null;

        public VisualizationComponent()
        {
            this.Visualization = DefaultVisualization;
        }

        /// <summary>
        ///   Visualization.
        /// </summary>
        [InspectorString(AttributeVisualization, Default = DefaultVisualization, Description = "Visualization.")]
        public string Visualization { get; set; }

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
        }
    }
}