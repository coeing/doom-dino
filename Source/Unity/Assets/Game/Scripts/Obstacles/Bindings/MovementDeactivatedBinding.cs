﻿namespace DoomedDino.Unity.Obstacles.Bindings
{
    using Slash.Unity.NDataExt.Bindings;

    using UnityEngine;

    public class MovementDeactivatedBinding : SimpleBinding<bool>
    {
        #region Methods

        protected override void ApplyNewValue(bool newValue)
        {
            base.ApplyNewValue(newValue);

            if (this.rigidbody2D != null)
            {
                if (newValue)
                {
                    this.rigidbody2D.Sleep();
                }
                else
                {
                    this.rigidbody2D.WakeUp();
                }
            }
        }

        #endregion
    }
}
