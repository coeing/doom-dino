﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollisionExitCommandBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Bindings
{
    using Slash.Unity.NDataExt.Bindings;

    using UnityEngine;

    public class TriggerExitCommandBinding : CommandBinding<TriggerExitCommandBinding.Delegate>
    {
        #region Delegates

        public delegate void Delegate(GameObject collider);

        #endregion

        #region Public Methods and Operators

        public void OnTriggerExit2D(Collider2D collision)
        {
            this.InvokeCommand(collision.gameObject);
        }

        #endregion
    }
}