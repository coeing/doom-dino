﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VisualizationBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Bindings
{
    using System.Collections.Generic;
    using System.Linq;

    using Slash.Unity.Common.Utils;
    using Slash.Unity.NDataExt.Bindings;

    using UnityEngine;

    public class VisualizationBinding : SimpleBinding<string>
    {
        #region Fields

        public List<GameObject> Prefabs;

        #endregion

        #region Methods

        protected override void ApplyNewValue(string newValue)
        {
            base.ApplyNewValue(newValue);

            this.CreateVisualization(newValue);
        }

        private void CreateVisualization(string prefabName)
        {
            GameObject visualizationPrefab = this.Prefabs.FirstOrDefault(prefab => prefab.name == prefabName);
            if (visualizationPrefab == null)
            {
                Debug.LogError("No prefab found for visualization " + prefabName + ".", this);
                return;
            }
            var layer = visualizationPrefab.layer;
            GameObject visualization = this.gameObject.AddChild(visualizationPrefab);
            visualization.layer = layer;
        }

        #endregion
    }
}