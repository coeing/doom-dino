﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpriteAnimationBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Bindings
{
    using System.Collections.Generic;
    using System.Linq;

    using DoomedDino.Unity.Animation;

    using Slash.Unity.NDataExt.Bindings;

    public class SpriteAnimationBinding : SimpleBinding<string>
    {
        #region Fields

        public List<SpriteAnimationData> Animations;

        public SpriteAnimation SpriteAnimation;

        #endregion

        #region Methods

        protected override void ApplyNewValue(string newValue)
        {
            base.ApplyNewValue(newValue);

            if (this.SpriteAnimation != null)
            {
                this.SpriteAnimation.Animation = this.Animations.FirstOrDefault(anim => anim.name == newValue);
            }
        }

        #endregion
    }
}