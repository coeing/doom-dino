﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddObstacleCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Bindings
{
    using DoomedDino.Unity.Game.Bindings;

    using UnityEngine;

    public class AddObstacleCommand : ControllerCommand<AddObstacleCommand.Delegate>
    {
        #region Fields

        public Transform PositionMarker;

        public string Type;

        #endregion

        #region Delegates

        public delegate void Delegate(string type);

        #endregion

        #region Methods

        protected override void OnButtonPress()
        {
            this.InvokeCommand(this.Type);
        }

        #endregion
    }
}