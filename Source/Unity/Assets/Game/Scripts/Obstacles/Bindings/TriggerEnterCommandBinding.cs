﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollisionEnterCommandBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Bindings
{
    using Slash.Unity.NDataExt.Bindings;

    using UnityEngine;

    public class TriggerEnterCommandBinding : CommandBinding<TriggerEnterCommandBinding.Delegate>
    {
        #region Delegates

        public delegate void Delegate(GameObject first, GameObject second);

        #endregion

        #region Public Methods and Operators

        public void OnTriggerEnter2D(Collider2D collision)
        {
            this.InvokeCommand(this.gameObject, collision.gameObject);
        }

        #endregion
    }
}