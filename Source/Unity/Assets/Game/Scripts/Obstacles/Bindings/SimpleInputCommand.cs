﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EndTheGameCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Bindings
{
    using DoomedDino.Unity.Game.Bindings;

    using EZData;

    public class SimpleInputCommand : ControllerCommand<Command>
    {
        #region Methods

        protected override void OnButtonPress()
        {
            this.InvokeCommand();
        }

        #endregion
    }
}