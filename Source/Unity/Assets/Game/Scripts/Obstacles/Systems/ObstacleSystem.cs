﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObstacleSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Logic.Obstacles.Systems
{
    using System.Collections.Generic;

    using DoomedDino.Logic.Obstacles.Components;
    using DoomedDino.Logic.Physics.Components;
    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Obstacles.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Blueprints;
    using Slash.ECS.Events;
    using Slash.ECS.Inspector.Attributes;
    using Slash.ECS.Systems;
    using Slash.Math.Algebra.Vectors;

    [GameSystem]
    [InspectorType]
    public class ObstacleSystem : GameSystem
    {
        #region Constants

        /// <summary>
        ///   Attribute: Armageddon blueprint.
        /// </summary>
        public const string AttributeArmageddonBlueprint = "ObstacleSystem.ArmageddonBlueprint";

        /// <summary>
        ///   Attribute: Armageddon offset
        /// </summary>
        public const string AttributeArmageddonOffset = "ObstacleSystem.ArmageddonOffset";

        /// <summary>
        ///   Attribute: Blueprint for obstacle target
        /// </summary>
        public const string AttributeBlueprintObstacleTarget = "ObstacleSystem.BlueprintObstacleTarget";

        /// <summary>
        ///   Attribute: Duration between placing obstacles
        /// </summary>
        public const string AttributeCooldownAddObstacle = "ObstacleSystem.CooldownAddObstacle";

        /// <summary>
        ///   Attribute: Duration to get an additional obstacle.
        /// </summary>
        public const string AttributeDurationAdditionalObstacle = "ObstacleSystem.DurationAdditionalObstacle";

        /// <summary>
        ///   Attribute: Duration to reload an obstacle
        /// </summary>
        public const string AttributeDurationReloadObstacle = "ObstacleSystem.DurationReloadObstacle";

        /// <summary>
        ///   Attribute: Maximum number of obstacles.
        /// </summary>
        public const string AttributeMaxNumObstacles = "ObstacleSystem.MaxNumObstacles";

        /// <summary>
        ///   Attribute: Obstacle target speed.
        /// </summary>
        public const string AttributeObstacleTargetSpeed = "ObstacleSystem.ObstacleTargetSpeed";

        /// <summary>
        ///   Attribute default: Armageddon blueprint.
        /// </summary>
        public const string DefaultArmageddonBlueprint = null;

        /// <summary>
        ///   Attribute default: Armageddon offset
        /// </summary>
        public const Vector2F DefaultArmageddonOffset = null;

        /// <summary>
        ///   Attribute default: Blueprint for obstacle target
        /// </summary>
        public const string DefaultBlueprintObstacleTarget = null;

        /// <summary>
        ///   Attribute default: Duration between placing obstacles
        /// </summary>
        public const float DefaultCooldownAddObstacle = 1.0f;

        /// <summary>
        ///   Attribute default: Duration to get an additional obstacle.
        /// </summary>
        public const float DefaultDurationAdditionalObstacle = 10.0f;

        /// <summary>
        ///   Attribute default: Duration to reload an obstacle
        /// </summary>
        public const float DefaultDurationReloadObstacle = 5.0f;

        /// <summary>
        ///   Attribute default: Maximum number of obstacles.
        /// </summary>
        public const int DefaultMaxNumObstacles = 10;

        /// <summary>
        ///   Attribute default: Obstacle target speed.
        /// </summary>
        public const float DefaultObstacleTargetSpeed = 8;

        #endregion

        #region Fields

        private readonly Queue<PlaceObstacle> placeObstacles = new Queue<PlaceObstacle>();

        /// <summary>
        ///   Time for additional obstacle (in s).
        /// </summary>
        private float additionalObstacle;

        private Blueprint armageddonBlueprint;

        private int armageddonEntityId;

        /// <summary>
        ///   Current available obstacles.
        /// </summary>
        private int availableObstacles;

        /// <summary>
        ///   Indicates if it's the end of the world.
        /// </summary>
        private bool isEndOfWorld;

        /// <summary>
        ///   Maximum available obstacles.
        /// </summary>
        private int maxObstacles;

        private Blueprint obstacleTargetBlueprint;

        private int obstacleTargetId;

        private PositionComponent obstacleTargetPositionComponent;

        /// <summary>
        ///   Time for reloading obstacle (in s).
        /// </summary>
        private float reloadObstacle;

        private float remainingCooldownAddObstacle;

        #endregion

        #region Public Properties

        /// <summary>
        ///   Armageddon blueprint.
        /// </summary>
        [InspectorBlueprint(AttributeArmageddonBlueprint, Description = "Armageddon blueprint.",
            Default = DefaultArmageddonBlueprint)]
        public string ArmageddonBlueprint { get; set; }

        /// <summary>
        ///   Armageddon offset
        /// </summary>
        [InspectorVector(AttributeArmageddonOffset, Description = "Armageddon offset", Default = DefaultArmageddonOffset
            )]
        public Vector2F ArmageddonOffset { get; set; }

        /// <summary>
        ///   Blueprint for obstacle target
        /// </summary>
        [InspectorBlueprint(AttributeBlueprintObstacleTarget, Description = "Blueprint for obstacle target",
            Default = DefaultBlueprintObstacleTarget)]
        public string BlueprintObstacleTarget { get; set; }

        /// <summary>
        ///   Duration between placing obstacles
        /// </summary>
        [InspectorFloat(AttributeCooldownAddObstacle, Description = "Duration between placing obstacles",
            Default = DefaultCooldownAddObstacle)]
        public float CooldownAddObstacle { get; set; }

        /// <summary>
        ///   Duration to get an additional obstacle.
        /// </summary>
        [InspectorFloat(AttributeDurationAdditionalObstacle, Description = "Duration to get an additional obstacle.",
            Default = DefaultDurationAdditionalObstacle)]
        public float DurationAdditionalObstacle { get; set; }

        /// <summary>
        ///   Duration to reload an obstacle
        /// </summary>
        [InspectorFloat(AttributeDurationReloadObstacle, Description = "Duration to reload an obstacle",
            Default = DefaultDurationReloadObstacle)]
        public float DurationReloadObstacle { get; set; }

        /// <summary>
        ///   Maximum number of obstacles.
        /// </summary>
        [InspectorInt(AttributeMaxNumObstacles, Description = "Maximum number of obstacles.",
            Default = DefaultMaxNumObstacles)]
        public int MaxNumObstacles { get; set; }

        /// <summary>
        ///   Obstacle target speed.
        /// </summary>
        [InspectorFloat(AttributeObstacleTargetSpeed, Description = "Obstacle target speed.",
            Default = DefaultObstacleTargetSpeed)]
        public float ObstacleTargetSpeed { get; set; }

        #endregion

        #region Properties

        /// <summary>
        ///   Current available obstacles.
        /// </summary>
        private int AvailableObstacles
        {
            get
            {
                return this.availableObstacles;
            }
            set
            {
                if (value == this.availableObstacles)
                {
                    return;
                }

                this.availableObstacles = value;

                this.Game.EventManager.QueueEvent(ObstacleEvent.AvailableObstaclesChanged, this.availableObstacles);
            }
        }

        /// <summary>
        ///   Maximum available obstacles.
        /// </summary>
        private int MaxObstacles
        {
            get
            {
                return this.maxObstacles;
            }
            set
            {
                if (value == this.maxObstacles)
                {
                    return;
                }

                this.maxObstacles = value;

                this.Game.EventManager.QueueEvent(ObstacleEvent.MaxAvailableObstaclesChanged, this.maxObstacles);
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(FrameworkEvent.GameStarted, this.OnGameStarted);
            this.Game.EventManager.RegisterListener(DoomEvent.MaxDoomReached, this.OnMaxDoomReached);
            this.Game.EventManager.RegisterListener(DoomAction.Armageddon, this.OnArmageddon);

            this.obstacleTargetBlueprint = this.Game.BlueprintManager.GetBlueprint(this.BlueprintObstacleTarget);
            this.armageddonBlueprint = this.Game.BlueprintManager.GetBlueprint(this.ArmageddonBlueprint);
            this.Game.EventManager.RegisterListener(ObstacleAction.AddComet, this.OnAddComet);
            this.Game.EventManager.RegisterListener(ObstacleEvent.CometContactEnter, this.OnCometContactEnter);

            this.Game.EventManager.QueueEvent(ObstacleEvent.ObstacleReloadTimeChanged, this.DurationReloadObstacle);
        }

        private void OnCometContactEnter(GameEvent e)
        {
            var data = (CometContactEnterData)e.EventData;

            // Create obstacle.
            var cometImpactEffectComponent = this.Game.EntityManager.GetComponent<CometImpactEffectComponent>(data.EntityId);
            this.CreateObstacle(cometImpactEffectComponent.ObstacleBlueprintId, data.Position);

            // Remove comet.
            this.Game.EntityManager.RemoveEntity(data.EntityId);
            this.Game.EventManager.QueueEvent(
                ObstacleEvent.CometRemoved, new ObstacleRemovedData { EntityId = data.EntityId });
        }

        private void OnAddComet(GameEvent e)
        {
            // Check if comet is available.
            if (this.isEndOfWorld || this.AvailableObstacles <= 0)
            {
                return;
            }

            AddObstacleData data = (AddObstacleData)e.EventData;

            // Decrease available comets.
            --this.AvailableObstacles;
            this.Game.EventManager.QueueEvent(ObstacleEvent.AvailableObstaclesChanged, this.AvailableObstacles);

            // Add to queue.
            this.placeObstacles.Enqueue(new PlaceObstacle { Type = data.Type });
        }

        private void CreateComet(string type, Vector2F position)
        {
            // Get blueprint for comet.
            Blueprint blueprint = this.GetBlueprint(type);
            if (blueprint == null)
            {
                this.Game.Log.Error("No blueprint for comet type " + type + ".");
                return;
            }

            // Create entity.
            AttributeTable configuration = new AttributeTable { { PositionComponent.AttributePosition, position } };
            int entityId = this.Game.EntityManager.CreateEntity(blueprint, configuration);
            this.Game.EventManager.QueueEvent(
                ObstacleEvent.CometAdded, new ObstacleAddedData { EntityId = entityId });
        }

        public override void UpdateSystem(float dt)
        {
            base.UpdateSystem(dt);

            if (this.AvailableObstacles < this.MaxObstacles)
            {
                this.reloadObstacle += dt;
                if (this.reloadObstacle >= this.DurationReloadObstacle)
                {
                    ++this.AvailableObstacles;
                    this.reloadObstacle = 0;
                }
            }

            if (this.MaxObstacles < this.MaxNumObstacles)
            {
                this.additionalObstacle += dt;
                if (this.additionalObstacle >= this.DurationAdditionalObstacle)
                {
                    ++this.MaxObstacles;
                    ++this.AvailableObstacles;
                    this.additionalObstacle = 0;
                }
            }

            if (this.remainingCooldownAddObstacle > 0)
            {
                this.remainingCooldownAddObstacle -= dt;
            }

            if (this.remainingCooldownAddObstacle <= 0 && this.placeObstacles.Count > 0)
            {
                // Create comet.
                PlaceObstacle placeObstacle = this.placeObstacles.Dequeue();
                this.CreateComet(placeObstacle.Type, new Vector2F(this.obstacleTargetPositionComponent.Position.X, 10));

                // Set cooldown timer.
                this.remainingCooldownAddObstacle = this.CooldownAddObstacle;
            }

            // Move obstacle target.
            this.obstacleTargetPositionComponent.Position += new Vector2F(this.ObstacleTargetSpeed * dt, 0);
        }

        #endregion

        #region Methods

        private void CreateObstacle(string type, Vector2F position)
        {
            // Get blueprint for obstacle.
            Blueprint blueprint = this.GetBlueprint(type);
            if (blueprint == null)
            {
                this.Game.Log.Error("No blueprint for obstacle type " + type + ".");
                return;
            }

            // Create entity.
            AttributeTable configuration = new AttributeTable { { PositionComponent.AttributePosition, position } };
            int entityId = this.Game.EntityManager.CreateEntity(blueprint, configuration);
            this.Game.EventManager.QueueEvent(
                ObstacleEvent.ObstacleAdded, new ObstacleAddedData { EntityId = entityId });
        }

        private Blueprint GetBlueprint(string type)
        {
            return this.Game.BlueprintManager.GetBlueprint(type);
        }

        private void OnArmageddon(GameEvent e)
        {
            if (this.armageddonEntityId > 0)
            {
                this.Game.Log.Error("Already started armageddon.");
                return;
            }

            if (!this.isEndOfWorld)
            {
                this.Game.Log.Error("Not the end of the world yet.");
                return;
            }

            // It's the end of the world...
            AttributeTable configuration = new AttributeTable
                {
                    {
                        PositionComponent.AttributePosition,
                        this.obstacleTargetPositionComponent.Position + this.ArmageddonOffset
                    }
                };
            this.armageddonEntityId = this.Game.EntityManager.CreateEntity(this.armageddonBlueprint, configuration);
            this.Game.EventManager.QueueEvent(
                ObstacleEvent.ObstacleAdded, new ObstacleAddedData { EntityId = this.armageddonEntityId });
            this.Game.EventManager.QueueEvent(
                DoomEvent.ArmageddonStart, new ObstacleAddedData { EntityId = this.armageddonEntityId });
        }

        private void OnGameStarted(GameEvent e)
        {
            // Set initial obstacles.
            this.MaxObstacles = 1;
            this.AvailableObstacles = this.MaxObstacles;

            // Create obstacle target.
            this.obstacleTargetId = this.Game.EntityManager.CreateEntity(this.obstacleTargetBlueprint);
            this.obstacleTargetPositionComponent =
                this.Game.EntityManager.GetComponent<PositionComponent>(this.obstacleTargetId);
        }

        private void OnMaxDoomReached(GameEvent e)
        {
            this.isEndOfWorld = true;
        }

        #endregion

        private class PlaceObstacle
        {
            #region Fields

            public string Type;

            #endregion
        }
    }
}