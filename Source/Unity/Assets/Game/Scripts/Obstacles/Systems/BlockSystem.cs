﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BlockSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Logic.Obstacles.Systems
{
    using DoomedDino.Unity.Movement.Events;
    using DoomedDino.Unity.Obstacles.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;

    [GameSystem]
    public class BlockSystem : GameSystem
    {
        #region Fields

        private bool dashing;

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(ObstacleEvent.BlockCollision, this.OnBlockCollision);
            this.Game.EventManager.RegisterListener(MovementEvent.DashStarted, this.OnDashStarted);
            this.Game.EventManager.RegisterListener(MovementEvent.DashEnded, this.OnDashEnded);
        }

        #endregion

        #region Methods

        private void OnBlockCollision(GameEvent e)
        {
            var blockEntityId = (int)e.EventData;

            if (!this.dashing)
            {
                // Forward event to systems as if obstacle was fully hit.
                this.Game.EventManager.QueueEvent(ObstacleEvent.ObstacleContactEnter, e.EventData);
            }

            // Remove block.
            //this.Game.EntityManager.RemoveEntity(blockEntityId);
            //this.Game.EventManager.QueueEvent(
            //    ObstacleEvent.ObstacleRemoved, new ObstacleRemovedData { EntityId = blockEntityId });
        }

        private void OnDashEnded(GameEvent e)
        {
            this.dashing = false;
        }

        private void OnDashStarted(GameEvent e)
        {
            this.dashing = true;
        }

        #endregion
    }
}