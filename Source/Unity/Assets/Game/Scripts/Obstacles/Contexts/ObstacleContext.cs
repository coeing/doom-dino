﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObstacleContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Contexts
{
    using DoomedDino.Logic.Obstacles.Components;
    using DoomedDino.Logic.Physics.Components;
    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Obstacles.Events;

    using EZData;

    using Slash.Math.Algebra.Vectors;

    using UnityEngine;

    public class ObstacleContext : GameContext
    {
        #region Fields

        public bool WasHit;

        private readonly Property<string> animationProperty = new Property<string>();

        private readonly Property<bool> movementDeactivatedProperty = new Property<bool>();

        private readonly Property<Vector3> positionProperty = new Property<Vector3>();

        #endregion

        #region Public Properties

        public string Animation
        {
            get
            {
                return this.animationProperty.GetValue();
            }
            set
            {
                this.animationProperty.SetValue(value);
            }
        }

        public Property<string> AnimationProperty
        {
            get
            {
                return this.animationProperty;
            }
        }

        public int EntityId { get; private set; }

        public bool MovementDeactivated
        {
            get
            {
                return this.movementDeactivatedProperty.GetValue();
            }
            set
            {
                this.movementDeactivatedProperty.SetValue(value);
            }
        }

        public Property<bool> MovementDeactivatedProperty
        {
            get
            {
                return this.movementDeactivatedProperty;
            }
        }

        public Vector3 Position
        {
            get
            {
                return this.positionProperty.GetValue();
            }
            set
            {
                this.positionProperty.SetValue(value);
            }
        }

        public Property<Vector3> PositionProperty
        {
            get
            {
                return this.positionProperty;
            }
        }

        public string Visualization { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void Init(int entityId)
        {
            this.EntityId = entityId;
            this.Visualization = this.Game.EntityManager.GetComponent<VisualizationComponent>(entityId).Visualization;
            this.Position =
                this.ConvertPosition(this.Game.EntityManager.GetComponent<PositionComponent>(entityId).Position);
        }

        public void OnArmageddonEnter(GameObject first, GameObject second)
        {
            this.Animation = "armageddon_explosion";
            this.MovementDeactivated = true;

            // Dispatch event.
            this.Game.EventManager.QueueEvent(DoomEvent.ArmageddonHit, this.EntityId);
        }

        public void OnBlockCollision(GameObject first, GameObject second)
        {
            if (!this.WasHit)
            {
                this.WasHit = true;

                // Dispatch event.
                this.Game.EventManager.QueueEvent(ObstacleEvent.BlockCollision, this.EntityId);
            }
        }

        public void OnCollisionEnter(GameObject first, GameObject second)
        {
            if (!this.WasHit)
            {
                this.WasHit = true;

                // Dispatch event.
                this.Game.EventManager.QueueEvent(ObstacleEvent.ObstacleContactEnter, this.EntityId);
            }
        }

        public void OnCollisionExit(GameObject collider)
        {
            // Dispatch event.
            this.Game.EventManager.QueueEvent(ObstacleEvent.ObstacleContactExit, this.EntityId);
        }

        #endregion

        #region Methods

        private Vector3 ConvertPosition(Vector2F position)
        {
            return new Vector3(position.X, position.Y, 0);
        }

        #endregion
    }
}