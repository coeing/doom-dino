﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CometsContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace DoomedDino.Unity.Obstacles.Contexts
{
    using System.Linq;

    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Obstacles.Events;

    using EZData;

    using Slash.ECS.Events;

    public class CometsContext : GameContext
    {
        private readonly Collection<CometContext> comets = new Collection<CometContext>(false);

        public CometsContext()
        {
            this.RegisterListener(ObstacleEvent.CometAdded, this.OnCometAdded);
            this.RegisterListener(ObstacleEvent.CometRemoved, this.OnCometRemoved);
        }

        public Collection<CometContext> Comets
        {
            get
            {
                return this.comets;
            }
        }

        private void OnCometAdded(GameEvent e)
        {
            ObstacleAddedData data = (ObstacleAddedData)e.EventData;
            CometContext cometContext = new CometContext();
            cometContext.Init(data.EntityId);
            this.Comets.Add(cometContext);
        }

        private void OnCometRemoved(GameEvent e)
        {
            ObstacleRemovedData data = (ObstacleRemovedData)e.EventData;
            CometContext cometContext =
                this.Comets.Items.FirstOrDefault(context => context.EntityId == data.EntityId);
            this.Comets.Remove(cometContext);
        }
    }
}