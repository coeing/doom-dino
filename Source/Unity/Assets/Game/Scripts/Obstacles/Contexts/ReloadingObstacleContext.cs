﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReloadingObstacleContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Contexts
{
    using EZData;

    public class ReloadingObstacleContext : Context
    {
        #region Fields

        private readonly Property<float> reloadTimeElapsedProperty = new Property<float>();

        private readonly Property<float> reloadTimeTotalProperty = new Property<float>();

        #endregion

        #region Public Properties

        public float ReloadTimeElapsed
        {
            get
            {
                return this.reloadTimeElapsedProperty.GetValue();
            }
            set
            {
                this.reloadTimeElapsedProperty.SetValue(value);
            }
        }

        public Property<float> ReloadTimeElapsedProperty
        {
            get
            {
                return this.reloadTimeElapsedProperty;
            }
        }

        public float ReloadTimeTotal
        {
            get
            {
                return this.reloadTimeTotalProperty.GetValue();
            }
            set
            {
                this.reloadTimeTotalProperty.SetValue(value);
            }
        }

        public Property<float> ReloadTimeTotalProperty
        {
            get
            {
                return this.reloadTimeTotalProperty;
            }
        }

        #endregion
    }
}