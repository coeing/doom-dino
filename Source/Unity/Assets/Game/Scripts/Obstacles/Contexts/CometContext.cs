﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CometContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Contexts
{
    using DoomedDino.Logic.Obstacles.Components;
    using DoomedDino.Logic.Physics.Components;
    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Obstacles.Events;

    using EZData;

    using Slash.Math.Algebra.Vectors;

    using UnityEngine;

    public class CometContext : GameContext
    {
        #region Fields

        private readonly Property<Vector3> positionProperty = new Property<Vector3>();

        private bool cometCollisionHandled;

        #endregion

        #region Public Properties

        public int EntityId { get; private set; }

        public Vector3 Position
        {
            get
            {
                return this.positionProperty.GetValue();
            }
            set
            {
                this.positionProperty.SetValue(value);
            }
        }

        public Property<Vector3> PositionProperty
        {
            get
            {
                return this.positionProperty;
            }
        }

        public string Visualization { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void Init(int entityId)
        {
            this.EntityId = entityId;
            this.Visualization = this.Game.EntityManager.GetComponent<VisualizationComponent>(entityId).Visualization;

            var logicPosition = this.Game.EntityManager.GetComponent<PositionComponent>(entityId).Position;
            var unityPosition = new Vector3(logicPosition.X, logicPosition.Y, 0.0f);
            this.Position = unityPosition;
        }

        public void OnCollisionEnter(GameObject first, GameObject second)
        {
            if (this.cometCollisionHandled)
            {
                return;
            }

            // Raise event.
            var data = new CometContactEnterData
                {
                    EntityId = this.EntityId,
                    Position = new Vector2F(first.transform.position.x, first.transform.position.z)
                };
            this.Game.EventManager.QueueEvent(ObstacleEvent.CometContactEnter, data);
            this.cometCollisionHandled = true;
        }

        #endregion
    }
}