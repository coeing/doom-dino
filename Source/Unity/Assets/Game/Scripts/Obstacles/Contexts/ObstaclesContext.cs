﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObstaclesContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Contexts
{
    using System.Linq;

    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Obstacles.Events;

    using EZData;

    using Slash.ECS.Events;

    public class ObstaclesContext : GameContext
    {
        #region Fields

        private readonly Property<int> availableObstaclesProperty = new Property<int>();

        private readonly Property<int> maxAvailableObstaclesProperty = new Property<int>();

        private readonly Collection<ObstacleContext> obstacles = new Collection<ObstacleContext>(false);

        private readonly Collection<ReloadingObstacleContext> reloadingObstacles =
            new Collection<ReloadingObstacleContext>(false);

        private float obstacleReloadTime;

        #endregion

        #region Constructors and Destructors

        public ObstaclesContext()
        {
            this.RegisterListener(ObstacleEvent.ObstacleAdded, this.OnObstacleAdded);
            this.RegisterListener(ObstacleEvent.ObstacleRemoved, this.OnObstacleRemoved);
            this.RegisterListener(ObstacleEvent.AvailableObstaclesChanged, this.OnAvailableObstaclesChanged);
            this.RegisterListener(ObstacleEvent.MaxAvailableObstaclesChanged, this.OnMaxAvailableObstaclesChanged);
            this.RegisterListener(ObstacleEvent.ObstacleReloadTimeChanged, this.OnObstacleReloadTimeChanged);
        }

        #endregion

        #region Public Properties

        public int AvailableObstacles
        {
            get
            {
                return this.availableObstaclesProperty.GetValue();
            }
            set
            {
                this.availableObstaclesProperty.SetValue(value);
            }
        }

        public Property<int> AvailableObstaclesProperty
        {
            get
            {
                return this.availableObstaclesProperty;
            }
        }

        public int MaxAvailableObstacles
        {
            get
            {
                return this.maxAvailableObstaclesProperty.GetValue();
            }
            set
            {
                this.maxAvailableObstaclesProperty.SetValue(value);
            }
        }

        public Property<int> MaxAvailableObstaclesProperty
        {
            get
            {
                return this.maxAvailableObstaclesProperty;
            }
        }

        public Collection<ObstacleContext> Obstacles
        {
            get
            {
                return this.obstacles;
            }
        }

        public Collection<ReloadingObstacleContext> ReloadingObstacles
        {
            get
            {
                return this.reloadingObstacles;
            }
        }

        #endregion

        #region Methods

        private void OnAvailableObstaclesChanged(GameEvent e)
        {
            this.AvailableObstacles = (int)e.EventData;

            var list = this.reloadingObstacles.Items.ToList();

            for (var i = 0; i < list.Count; ++i)
            {
                list[i].ReloadTimeElapsed = i < this.AvailableObstacles ? list[i].ReloadTimeTotal : 0.0f;
            }
        }

        private void OnMaxAvailableObstaclesChanged(GameEvent e)
        {
            this.MaxAvailableObstacles = (int)e.EventData;

            while (this.reloadingObstacles.Count < this.MaxAvailableObstacles)
            {
                this.reloadingObstacles.Add(
                    new ReloadingObstacleContext { ReloadTimeTotal = this.obstacleReloadTime, ReloadTimeElapsed = 0.0f });
            }
        }

        private void OnObstacleAdded(GameEvent e)
        {
            ObstacleAddedData data = (ObstacleAddedData)e.EventData;
            ObstacleContext obstacleContext = new ObstacleContext();
            obstacleContext.Init(data.EntityId);
            this.Obstacles.Add(obstacleContext);
        }

        private void OnObstacleReloadTimeChanged(GameEvent e)
        {
            this.obstacleReloadTime = (float)e.EventData;
        }

        private void OnObstacleRemoved(GameEvent e)
        {
            ObstacleRemovedData data = (ObstacleRemovedData)e.EventData;
            ObstacleContext obstacleContext =
                this.Obstacles.Items.FirstOrDefault(context => context.EntityId == data.EntityId);
            this.Obstacles.Remove(obstacleContext);
        }

        #endregion
    }
}