﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaboteurContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Contexts
{
    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Game;
    using DoomedDino.Unity.Obstacles.Events;

    public class SaboteurContext : GameContext
    {
        #region Public Methods and Operators

        public void OnAddComet(string type)
        {
            this.Game.EventManager.QueueEvent(ObstacleAction.AddComet, new AddObstacleData { Type = type });
        }

        public void OnAddObstacle(string type)
        {
            this.Game.EventManager.QueueEvent(ObstacleAction.AddObstacle, new AddObstacleData { Type = type });
        }

        public void OnArmageddon()
        {
            this.Game.EventManager.QueueEvent(DoomAction.Armageddon);
        }

        #endregion
    }
}