﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObstacleAction.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace DoomedDino.Unity.Obstacles.Events
{
    public enum ObstacleAction
    {
        AddObstacle,

        AddComet
    }
}