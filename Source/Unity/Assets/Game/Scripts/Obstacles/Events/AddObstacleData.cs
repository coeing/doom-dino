﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddObstacleData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Events
{
    public class AddObstacleData
    {
        #region Public Properties

        public string Type { get; set; }

        #endregion
    }
}