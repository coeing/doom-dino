﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObstacleAddedData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Events
{
    public class ObstacleAddedData
    {
        #region Public Properties

        public int EntityId { get; set; }

        #endregion
    }
}