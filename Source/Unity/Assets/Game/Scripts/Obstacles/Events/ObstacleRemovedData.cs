﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObstacleRemovedData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Events
{
    public class ObstacleRemovedData
    {
        #region Public Properties

        public int EntityId { get; set; }

        #endregion
    }
}