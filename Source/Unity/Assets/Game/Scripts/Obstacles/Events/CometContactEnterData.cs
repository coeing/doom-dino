﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CometContactEnterData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Events
{
    using Slash.Math.Algebra.Vectors;

    public class CometContactEnterData
    {
        #region Public Properties

        public int EntityId { get; set; }

        public Vector2F Position { get; set; }

        #endregion
    }
}