﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObstacleEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Obstacles.Events
{
    public enum ObstacleEvent
    {
        ObstacleContactEnter,

        ObstacleContactExit,

        ObstacleAdded,

        ObstacleRemoved,

        AvailableObstaclesChanged,

        MaxAvailableObstaclesChanged,

        CometAdded,

        CometRemoved,

        CometContactEnter,

        CometContactExit,

        BlockCollision,

        ObstacleReloadTimeChanged
    }
}