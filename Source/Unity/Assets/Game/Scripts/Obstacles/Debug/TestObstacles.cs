﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestObstacles.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Assets.Game.Scripts.Obstacles.Debug
{
    using System;
    using System.Collections.Generic;

    using DoomedDino.Logic.Obstacles.Components;
    using DoomedDino.Logic.Physics.Components;
    using DoomedDino.Unity.Obstacles.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Blueprints;
    using Slash.ECS.Events;
    using Slash.Math.Algebra.Vectors;
    using Slash.Unity.Common.ECS;

    public class TestObstacles : GameEventBehaviour
    {
        #region Methods

        protected override void RegisterListeners()
        {
            base.RegisterListeners();
            this.RegisterListener(FrameworkEvent.GameStarted, this.OnGameStarted);
        }

        private void OnGameStarted(GameEvent e)
        {
            Blueprint blueprint = new Blueprint
                {
                    ComponentTypes = new List<Type> { typeof(PositionComponent), typeof(VisualizationComponent) }
                };
            AttributeTable configuration = new AttributeTable
                {
                    { PositionComponent.AttributePosition, Vector2F.Zero },
                    { VisualizationComponent.AttributeVisualization, "DoJump" }
                };
            int entityId = this.Game.EntityManager.CreateEntity(blueprint, configuration);
            this.Game.EventManager.QueueEvent(
                ObstacleEvent.ObstacleAdded, new ObstacleAddedData { EntityId = entityId });
        }

        #endregion
    }
}