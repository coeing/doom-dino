﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DoomSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Doom.Systems
{
    using System;

    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Floors.Events;
    using DoomedDino.Unity.Game.Events;
    using DoomedDino.Unity.Obstacles.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Blueprints;
    using Slash.ECS.Events;
    using Slash.ECS.Inspector.Attributes;
    using Slash.ECS.Systems;

    [InspectorType]
    [GameSystem]
    public class DoomSystem : GameSystem
    {
        #region Constants

        /// <summary>
        ///   Attribute: Doom increase per floor revealed.
        /// </summary>
        public const string AttributeDoomPerFloor = "DoomSystem.DoomPerFloor";

        /// <summary>
        ///   Attribute: Doom increase per obstacle collision.
        /// </summary>
        public const string AttributeDoomPerObstacle = "DoomSystem.DoomPerObstacle";

        /// <summary>
        ///   Attribute: Maximum doom.
        /// </summary>
        public const string AttributeMaxDoom = "DoomSystem.MaxDoom";

        /// <summary>
        ///   Attribute default: Doom increase per floor revealed.
        /// </summary>
        public const float DefaultDoomPerFloor = 0.1f;

        /// <summary>
        ///   Attribute default: Doom increase per obstacle collision.
        /// </summary>
        public const float DefaultDoomPerObstacle = 0.1f;

        /// <summary>
        ///   Attribute default: Maximum doom.
        /// </summary>
        public const float DefaultMaxDoom = 100;

        #endregion

        #region Fields

        private float currentDoom;


        #endregion

        #region Public Properties

        /// <summary>
        ///   Doom increase per floor revealed.
        /// </summary>
        [InspectorFloat(AttributeDoomPerFloor, Default = DefaultDoomPerFloor,
            Description = "Doom increase per floor revealed.")]
        public float DoomPerFloor { get; set; }

        /// <summary>
        ///   Doom increase per obstacle collision.
        /// </summary>
        [InspectorFloat(AttributeDoomPerObstacle, Default = DefaultDoomPerObstacle,
            Description = "Doom increase per obstacle collision.")]
        public float DoomPerObstacle { get; set; }

        /// <summary>
        ///   Maximum doom.
        /// </summary>
        [InspectorFloat(AttributeMaxDoom, Description = "Maximum doom.", Default = DefaultMaxDoom)]
        public float MaxDoom { get; set; }

        #endregion

        #region Properties

        private float CurrentDoom
        {
            get
            {
                return this.currentDoom;
            }
            set
            {
                if (Math.Abs(value - this.currentDoom) < 0.0001f)
                {
                    return;
                }

                float oldDoom = this.currentDoom;
                this.currentDoom = value;

                this.Game.EventManager.QueueEvent(DoomEvent.CurrentDoomChanged, this.CurrentDoom);

                if (oldDoom < this.MaxDoom && this.currentDoom >= this.MaxDoom)
                {
                    this.OnMaxDoomReached();
                }
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(FloorEvent.FloorBecameVisible, this.OnFloorBecameVisible);
            this.Game.EventManager.RegisterListener(ObstacleEvent.ObstacleContactEnter, this.OnObstacleContactEnter);
            this.Game.EventManager.RegisterListener(DoomEvent.ArmageddonHit, this.OnArmageddonHit);
        }

        private void OnArmageddonHit(GameEvent e)
        {
            // End the game.
            this.Game.PauseGame();

            this.Game.EventManager.QueueEvent(DoomedDinoEvent.GameFinished);
        }

        #endregion

        #region Methods

        private void OnFloorBecameVisible(GameEvent e)
        {
            this.CurrentDoom += this.DoomPerFloor;
        }

        private void OnMaxDoomReached()
        {
            this.Game.EventManager.QueueEvent(DoomEvent.MaxDoomReached);
        }

        private void OnObstacleContactEnter(GameEvent e)
        {
            this.CurrentDoom += this.DoomPerObstacle;
        }

        #endregion
    }
}