﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DoomEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Doom.Events
{
    public enum DoomEvent
    {
        CurrentDoomChanged,

        MaxDoomReached,

        ArmageddonHit,

        ArmageddonStart
    }
}