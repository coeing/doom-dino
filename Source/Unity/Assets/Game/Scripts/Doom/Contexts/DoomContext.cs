﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DoomContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Doom.Contexts
{
    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Game;

    using EZData;

    using Slash.ECS.Events;

    public class DoomContext : GameContext
    {
        #region Fields

        private readonly Property<float> currentDoomProperty = new Property<float>();

        #endregion

        #region Constructors and Destructors

        public DoomContext()
        {
            this.RegisterListener(DoomEvent.CurrentDoomChanged, this.OnCurrentDoomChanged);
        }

        #endregion

        #region Public Properties

        public float CurrentDoom
        {
            get
            {
                return this.currentDoomProperty.GetValue();
            }
            set
            {
                this.currentDoomProperty.SetValue(value);
            }
        }

        public Property<float> CurrentDoomProperty
        {
            get
            {
                return this.currentDoomProperty;
            }
        }

        #endregion

        #region Methods

        private void OnCurrentDoomChanged(GameEvent e)
        {
            this.CurrentDoom = (float)e.EventData;
        }

        #endregion
    }
}