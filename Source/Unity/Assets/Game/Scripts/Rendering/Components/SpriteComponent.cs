﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpriteComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Rendering.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class SpriteComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Name of the sprite of this entity.
        /// </summary>
        public const string AttributeSpriteName = "SpriteComponent.SpriteName";

        /// <summary>
        ///   Attribute default: Name of the sprite of this entity.
        /// </summary>
        public const string DefaultSpriteName = "";

        #endregion

        #region Constructors and Destructors

        public SpriteComponent()
        {
            this.SpriteName = DefaultSpriteName;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Name of the sprite of this entity.
        /// </summary>
        [InspectorString(AttributeSpriteName, Default = DefaultSpriteName,
            Description = "Name of the sprite of this entity.")]
        public string SpriteName { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.SpriteName = attributeTable.GetValueOrDefault(AttributeSpriteName, DefaultSpriteName);
        }

        #endregion
    }
}