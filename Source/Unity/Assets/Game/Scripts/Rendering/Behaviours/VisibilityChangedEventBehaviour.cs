﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VisibilityChangedEventBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Assets.Game.Scripts.Rendering.Behaviours
{
    using System;

    using UnityEngine;

    public class VisibilityChangedEventBehaviour : MonoBehaviour
    {
        #region Public Events

        public event Action<GameObject> BecameInvisible;

        public event Action<GameObject> BecameVisible;

        #endregion

        #region Methods

        private void OnBecameInvisible()
        {
            var handler = this.BecameInvisible;
            if (handler != null)
            {
                handler(this.gameObject);
            }
        }

        private void OnBecameVisible()
        {
            var handler = this.BecameVisible;
            if (handler != null)
            {
                handler(this.gameObject);
            }
        }

        #endregion
    }
}