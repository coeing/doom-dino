﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HowToPlayViewModel.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.UI.HowToPlay
{
    using EZData;

    using UnityEngine;

    public class HowToPlayViewModel : Context
    {
        #region Public Methods and Operators

        public void OnBackClicked()
        {
            Application.LoadLevel("MainMenuScene");
        }

        #endregion
    }
}