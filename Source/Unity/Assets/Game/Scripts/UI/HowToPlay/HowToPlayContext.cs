﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HowToPlayContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.UI.HowToPlay
{
    using UnityEngine;

    public class HowToPlayContext : MonoBehaviour
    {
        #region Fields

        public HowToPlayViewModel Context;

        public NguiRootContext View;

        #endregion

        #region Methods

        private void Awake()
        {
            this.Context = new HowToPlayViewModel();
            this.View.SetContext(this.Context);
        }

        #endregion
    }
}