﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditsViewModel.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.UI.Credits
{
    using EZData;

    using UnityEngine;

    public class CreditsViewModel : Context
    {
        #region Public Methods and Operators

        public void OnBackClicked()
        {
            Application.LoadLevel("MainMenuScene");
        }

        #endregion
    }
}