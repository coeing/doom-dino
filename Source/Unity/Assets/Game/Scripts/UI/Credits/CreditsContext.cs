﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditsContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.UI.Credits
{
    using UnityEngine;

    public class CreditsContext : MonoBehaviour
    {
        #region Fields

        public CreditsViewModel Context;

        public NguiRootContext View;

        #endregion

        #region Methods

        private void Awake()
        {
            this.Context = new CreditsViewModel();
            this.View.SetContext(this.Context);
        }

        #endregion
    }
}