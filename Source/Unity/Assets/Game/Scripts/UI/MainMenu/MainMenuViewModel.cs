﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainMenuViewModel.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.UI.MainMenu
{
    using EZData;

    using UnityEngine;

    public class MainMenuViewModel : Context
    {
        #region Public Methods and Operators

        public void OnCreditsClicked()
        {
            Application.LoadLevel("CreditsScene");
        }

        public void OnHowToPlayClicked()
        {
            Application.LoadLevel("HowToPlayScene");
        }

        public void OnPlayClicked()
        {
            Application.LoadLevel("GameScene");
        }

        public void OnQuitClicked()
        {
            Application.Quit();
        }

        #endregion
    }
}