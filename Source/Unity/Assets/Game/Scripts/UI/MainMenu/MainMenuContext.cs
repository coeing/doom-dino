﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainMenuContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.UI.MainMenu
{
    using UnityEngine;

    public class MainMenuContext : MonoBehaviour
    {
        #region Fields

        public MainMenuViewModel Context;

        public NguiRootContext View;

        #endregion

        #region Methods

        private void Awake()
        {
            this.Context = new MainMenuViewModel();
            this.View.SetContext(this.Context);
        }

        #endregion
    }
}