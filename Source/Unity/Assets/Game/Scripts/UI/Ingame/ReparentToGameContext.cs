﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReparentToGameContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.UI.Ingame
{
    using DoomedDino.Unity.Game;

    using UnityEngine;

    public class ReparentToGameContext : MonoBehaviour
    {
        #region Methods

        /// <summary>
        ///   Called before first Update call.
        /// </summary>
        private void Start()
        {
            var gameContext = FindObjectOfType<DoomedDinoGameViewModel>();
            this.transform.parent = gameContext.transform;
        }

        #endregion
    }
}