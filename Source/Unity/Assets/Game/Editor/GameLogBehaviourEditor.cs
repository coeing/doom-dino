﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameLogBehaviourEditor.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DoomedDino.Unity.Editor
{
    using System;
    using System.Collections.Generic;

    using DoomedDino.Unity.Doom.Events;
    using DoomedDino.Unity.Floors.Events;
    using DoomedDino.Unity.Movement.Events;
    using DoomedDino.Unity.Obstacles.Events;
    using DoomedDino.Unity.Score.Events;

    using Slash.ECS.Events;
    using Slash.Unity.Common.Logging;
    using Slash.Unity.Editor.Common.Inspectors.Logging;

    using UnityEditor;

    [CustomEditor(typeof(GameLogBehaviour))]
    public class GameLogBehaviourEditor : EventManagerLogBehaviourEditor
    {
        #region Properties

        protected override IEnumerable<Type> EventTypes
        {
            get
            {
                return new[]
                    { typeof(FloorEvent), typeof(ObstacleEvent), typeof(FrameworkEvent), typeof(MovementEvent), typeof(DoomEvent), typeof(ScoreEvent) };
            }
        }

        #endregion
    }
}