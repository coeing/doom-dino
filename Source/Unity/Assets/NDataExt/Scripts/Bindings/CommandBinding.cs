﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.NDataExt.Bindings
{
    using System;

    using UnityEngine;

    public class CommandBinding<TDelegate> : NguiBinding
    {
        #region Fields

        protected Delegate Command;

        #endregion

        #region Public Methods and Operators

        public void InvokeCommand(params object[] args)
        {
            if (this.Command != null)
            {
                this.Command.DynamicInvoke(args);
            }
        }

        #endregion

        #region Methods

        protected override void Bind()
        {
            base.Bind();

            var context = this.GetContext(this.Path);
            if (context == null)
            {
                return;
            }

            this.Command = context.FindCommand<TDelegate>(this.Path, this);
            if (this.Command == null)
            {
                Debug.LogWarning("No command found at " + this.Path);
            }
        }

        protected override void Unbind()
        {
            base.Unbind();

            this.Command = null;
        }

        #endregion
    }
}