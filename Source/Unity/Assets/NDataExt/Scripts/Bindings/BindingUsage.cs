﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BindingUsage.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.NDataExt.Bindings
{
    using UnityEngine;

    public abstract class BindingUsage<T> : MonoBehaviour
    {
        #region Fields

        public NguiBaseBinding Binding;

        #endregion

        #region Methods

        protected abstract void OnValueChanged(T newValue);

        private void OnDisable()
        {
            GenericValueBinding<T> valueBinding = this.Binding as GenericValueBinding<T>;
            if (valueBinding != null)
            {
                valueBinding.ValueChanged -= this.OnValueChanged;
            }
        }

        private void OnEnable()
        {
            GenericValueBinding<T> valueBinding = this.Binding as GenericValueBinding<T>;
            if (valueBinding != null)
            {
                valueBinding.ValueChanged += this.OnValueChanged;
                this.OnValueChanged(valueBinding.Value);
            }
        }

        #endregion
    }
}