﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimpleFloatBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.NDataExt.Bindings
{
    public class SimpleFloatBinding : SimpleBinding<float>
    {
    }
}