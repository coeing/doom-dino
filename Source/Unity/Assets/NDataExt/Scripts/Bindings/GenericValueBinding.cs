﻿namespace Slash.Unity.NDataExt.Bindings
{
    public abstract class GenericValueBinding<T> : NguiBaseBinding
    {
        #region Fields

        private T value;

        #endregion

        #region Delegates

        public delegate void ValueChangedDelegate(T newValue);

        #endregion

        #region Public Events

        public event ValueChangedDelegate ValueChanged;

        #endregion

        #region Public Properties

        public T Value
        {
            get
            {
                return this.value;
            }
            protected set
            {
                if (Equals(value, this.value))
                {
                    return;
                }

                this.value = value;
                this.OnValueChanged(this.value);
            }
        }

        #endregion

        #region Methods

        private void OnValueChanged(T newValue)
        {
            this.Value = newValue;

            ValueChangedDelegate handler = this.ValueChanged;
            if (handler != null)
            {
                handler(newValue);
            }
        }

        #endregion
    }
}