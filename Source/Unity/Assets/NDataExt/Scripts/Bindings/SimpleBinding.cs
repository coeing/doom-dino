﻿namespace Slash.Unity.NDataExt.Bindings
{
    using System.Collections.Generic;

    using EZData;

    using UnityEngine;
    
    public abstract class SimpleBinding<T> : GenericValueBinding<T>
    {
        #region Fields

        public T DefaultValue;

        public string Path;

        private bool ignoreValueChange;

        private Property<T> property;

        #endregion

        #region Public Properties

        public override IList<string> ReferencedPaths
        {
            get
            {
                return new[] { this.Path };
            }
        }

        #endregion

        #region Methods

        protected virtual void ApplyInputValue(T inputValue)
        {
            this.ignoreValueChange = true;

            if (this.property != null)
            {
                this.property.SetValue(inputValue);
            }

            this.ignoreValueChange = false;
        }

        protected virtual void ApplyNewValue(T newValue)
        {
            this.Value = newValue;
        }

        protected override void Bind()
        {
            base.Bind();

            var context = this.GetContext(this.Path);
            if (context != null)
            {
                this.property = context.FindProperty<T>(this.Path, this);
                if (this.property != null)
                {
                    this.property.OnChange += this.OnChange;
                }
                else
                {
                    Debug.LogWarning(
                        "No property of type '" + typeof(T) + "' found at path '" + this.GetFullCleanPath(this.Path)
                        + "'.",
                        this);
                }
            }
            else
            {
                Debug.LogWarning("No context found for path " + this.Path, this);
            }
        }

        protected override void OnChange()
        {
            base.OnChange();

            var newValue = this.DefaultValue;

            if (this.property != null)
            {
                newValue = this.property.GetValue();
            }

            if (!this.ignoreValueChange)
            {
                this.ApplyNewValue(newValue);
            }
        }

        protected override void Unbind()
        {
            base.Unbind();

            if (this.property != null)
            {
                this.property.OnChange -= this.OnChange;
                this.property = null;
            }
        }

        #endregion
    }
}