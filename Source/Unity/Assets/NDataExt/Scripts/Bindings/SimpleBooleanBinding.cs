﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimpleBooleanBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.NDataExt.Bindings
{
    using NDataExt.Bindings;

    public class SimpleBooleanBinding : SimpleBinding<bool>
    {
        #region Fields

        public bool Invert;

        #endregion

        #region Methods

        protected override void ApplyInputValue(bool inputValue)
        {
            base.ApplyInputValue(this.Invert ? (!inputValue) : inputValue);
        }

        protected override void ApplyNewValue(bool newValue)
        {
            base.ApplyNewValue(this.Invert ? (!newValue) : newValue);
        }

        #endregion
    }
}