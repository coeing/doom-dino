﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConditionalColorBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.NDataExt.Bindings
{
    using UnityEngine;

    public class ConditionalColorBinding : NguiBooleanBinding
    {
        #region Fields

        public Color ColorIfFalse = Color.white;

        public Color ColorIfTrue = Color.white;

        public UIWidget Target;

        #endregion

        #region Public Methods and Operators

        public override void Awake()
        {
            base.Awake();

            if (this.Target == null)
            {
                this.Target = this.GetComponent<UIWidget>();
            }
        }

        #endregion

        #region Methods

        protected override void ApplyNewValue(bool newValue)
        {
            this.Target.color = newValue ? this.ColorIfTrue : this.ColorIfFalse;
        }

        #endregion
    }
}