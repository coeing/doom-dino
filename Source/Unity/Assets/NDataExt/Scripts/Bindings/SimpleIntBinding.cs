﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimpleIntBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.NDataExt.Bindings
{
    public class SimpleIntBinding : SimpleBinding<int>
    {
    }
}