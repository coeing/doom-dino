﻿namespace Slash.Unity.NDataExt.Bindings
{
    using Slash.Math.Algebra.Vectors;

    public class SimpleVector2IBinding : SimpleBinding<Vector2I>
    {
    }
}